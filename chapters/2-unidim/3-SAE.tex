\chapter{Utilisation d'autoencodeurs empilés pour le débruitage non supervisé de documents parlés}
\label{chap:sae}
\minitoc
\section{Introduction}
\label{chap:sae:sec:introduction}

Pour résoudre les problématiques de SLU, de nombreuses solutions appliquent des méthodologies de recherche d'informations directement sur les hypothèses générées par un SRAP~\cite{leelee}. \cleaned{Ces méthodes sont conçues initialement} pour traiter des sources textuelles très peu bruitées. Celles-ci fonctionnent bien dans le cadre de parole préparée comme des reportages radiophoniques et télédiffusés, mais elles ont plus de difficultés pour traiter la parole spontanée. En effet, la parole spontanée apporte deux différences fondamentales. La première vient des reprises, contradictions, répétitions et de tous les défauts de langage (disfluences) qui font la particularité de la parole spontanée.
La seconde différence est liée au SRAP. Celui-ci est un système statistique limité qui peut produire une quantité importante d'erreurs lors de la génération des hypothèses de transcription. Ces erreurs sont dues à l'environnement sonore particulier du document, à la taille limitée du vocabulaire du système, et à la présence de plusieurs locuteurs qui parlent simultanément\etc

%Pour résoudre des problématiques de SLU, de nombreuses propositions se contentent d'associer un SRAP et un moteur de recherche d'information~\cite{Lee2015SpokenCR}. Les hypothèses produites par un SRAP ont des caractéristiques propres très différentes du langage écrit qui est habituellement traité par les méthodes de recherche d'informations.
%Ces différences sont dues à deux raisons principales~: les transcriptions représentent du langage oral et celles-ci sont générées par un SRAP. Le langage oral est moins structuré que l'écrit, les disfluences verbales y sont très présentes, même dans le cadre de la parole lue. Il contient aussi des modalités supplémentaires qui ne sont pas exploitables à l'écrit. Leurs absence rend l'interprétation de transcription complexe. Quant aux SRAP, ils génèrent des hypothèses de transcriptions. Ces hypothèses peuvent être multiples pour une séquence de signal donnée. La génération de celles-ci repose sur des modèles statistiques. Ces modèles peuvent introduire des biais lors de leurs apprentissages qui affecteront les transcriptions notamment lorsque le SRAP commet une erreur.


Les travaux présentés dans ce chapitre reposent sur l'hypothèse que les erreurs introduites par le SRAP sont un bruit qu'un réseau de neurones artificiels peut modéliser et \cleaned{gommer} de la représentation. Nous proposons ainsi une méthode visant à réduire l'impact du processus de transcription automatique lors de la classification des segments transcrits.
Cette méthode est appliquée entre le SRAP et la phase de classification comme montrée dans la Figure~\ref{chap:decoda:fig:pg_decoda} du Chapitre~\ref{chap:decoda}. Les sorties de SRAP sont représentées par des sac-de-mots avec des descripteurs TF.IDF.GINI introduits aussi dans le Chapitre~\ref{chap:decoda}.

Les réseaux de neurones se sont récemment positionnés comme les méthodes les plus efficaces pour la réduction du bruit, notamment sur le traitement de la parole~\cite{feng2014speech} et des tâches d'extractions d'informations depuis des tweets~\cite{liu2015estimating}.
Dans ces travaux, nous proposons deux approches différentes, qui exploitent les capacités à compresser l'information pour générer une représentation robuste.

La première approche consiste à utiliser des autoencodeurs empilés (Stacked autoencoder,SAE)~\cite{vincent2010stacked} pour produire de nouvelles représentations des documents. Utiliser un réseau plus profond permet de déduire et d'exploiter des concepts de plus haut niveau (plus abstraits)~\cite{bengio2009learning}. Dans le cadre d'un SAE, les couches cachées ont les mêmes caractéristiques qu'un autoencodeur débruitant. Cette spécificité permet aux couches profondes de produire des représentations plus abstraites, mais aussi plus robustes aux bruits.
Les représentations latentes inférées par un SAE sont réalisées de manière à conserver toute l'information nécessaire pour pouvoir reconstruire le document d'origine. Elles sont donc : d'un plus haut niveau d'abstraction, plus robustes et aussi informatives que les données d'origine. Un classifieur entrainé sur celles-ci généralise mieux et obtient de meilleurs résultats~\cite{hinton2006reducing,bengio2007greedy}.

La seconde considère les transcriptions générées par un SRAP comme des documents corrompus  (ASR) et les transcriptions manuelles (TRS) comme la version propre des mêmes documents.
Nous proposons d'utiliser un autoencodeur débruitant (DAE) dont le vecteur d'entrée n'est plus bruité artificiellement, mais utilise le bruit <<naturel>> introduit par le SRAP.
Un DAE est entrainé pour supprimer le bruit des documents issus du SRAP. La variabilité introduite par les erreurs du SRAP est réduite en entrainant le réseau à reconstruire la même représentation, mais issue de transcriptions manuelles. C'est la principale différence avec  la première méthode qui n'impose pas de supervision avec les transcriptions manuelles pour la reconstruction.

%MOCHE
%\section{Motivations}
%\label{chap:sae:sec:relatedWorks}
Les premières applications de réseaux de neurones capables de reconstruire des données à partir d'une représentation latente sont définies dans \cite{le1987modeles} et~\cite{gallinari1987memoires}. Plus récemment \cite{hinton2006reducing} proposent des autoencodeurs profonds et une méthode d'apprentissage couche par couche. Cet apprentissage est réalisé à partir de poids appris sans supervision par des autoencodeurs ``simples''(non profonds). Cette méthode permet de construire des représentations de tailles réduites contenant suffisamment d'informations pour reconstruire les documents d'origines. Des résultats intéressants sont obtenus quand l'apprentissage par couche est suivi d'un apprentissage global (dit fine tuning)~\cite{erhan2010does}.

Dans le but d'améliorer l'efficacité et la stabilité des représentations obtenues avec un AE, des DAE sont proposés~\cite{vincent2008}. Ils sont entrainés de manière à réduire la variabilité de la représentation de la couche $k$ lorsque le bruit est introduit dans la couche précédente ($k - 1$).
Les représentations compressées, construites par des DAE, permettent aux systèmes automatiques de mieux généraliser et d'être résistants aux variabilités qui ne sont pas porteuses d'informations. Ces propriétés ont permis d'obtenir de bons résultats dans différents domaines comme la médecine~\cite{xu2015stacked}, la biologie~\cite{camacho2015feature}, le traitement de l'image ~\cite{maria2015stacked}, le traitement de la musique ~\cite{sarroff2014musical} et le traitement de la parole~\cite{chao2014improving}.
Lors de l'estimation des poids optimaux de ces réseaux, les vecteurs d'entrée sont corrompus artificiellement par un bruit additif. Le réseau apprend ainsi à séparer le bruit de l'information importante. Dans la littérature, les DAE génèrent de meilleures représentations que les machines de Boltzmann restreintes, les SVM et les autoencodeurs sans bruit additif~\cite{vincent2010stacked,tan2008performance}.

Dans \cite{lu2013speech}, un DAE basé sur un autoencodeur empilé est proposé pour reconstruire des coefficients cepstraux de fréquence en échelle de Mel (MFCC) propres à partir de leurs versions bruitées.
Dans \cite{feng2014speech} un DAE et un AE <<antiréverbération>> sont proposés pour compresser et supprimer la réverbération de signaux audios. Un DAE est aussi proposé pour débruiter un spectrogramme dans \cite{deng2010binary}.
Des résultats intéressants ont été obtenus avec des SAE pour l'extraction de caractéristiques acoustiques dans \cite{seide2011conversational}. Un SRAP dont les caractéristiques acoustiques sont adaptées par les couches cachées d'un réseau de neurones profonds est présenté dans \cite{yu2013feature, sainath2012auto}. Cette construction de descripteurs acoustiques permet d'obtenir de meilleurs résultats que des alternatives classiques (GMM, fMLLR et VTLN).  Un SRAP robuste qui repose sur des caractéristiques articulatoires apprises par réseaux de neurones profonds combinés aux usuelles MFCC, est introduit dans \cite{vinyals2011comparing}.

%L'objectif de l'approche proposé dans ce chapitre est la reconstruction de caractéristiques issue des informations produite par un SRAP pour n'en conservé que la sélection la plus significative. On peut voir les erreurs introduites par le SRAP comme un bruit et les autoencodeurs débruitant sont utilisés pour minimiser l'impact de ce bruit.

L'approche proposée dans ce chapitre consiste à considérer les erreurs introduites par le SRAP comme un bruit. Ainsi, des autoencodeurs sont utilisés pour construire des représentations de l'information transcrites automatiquement qui sont robustes au bruit et donc facilitent l'extraction d'informations thématiques.




\section{Fonctionnement des autoencodeurs}
\label{chap:sae:sec:Autoencoder}
En premier lieu, le fonctionnement des autoencodeurs est détaillé. Ensuite des variantes particulières des autoencodeurs sont définies : le DAE et l'autoencodeur empilé~(SAE).

\subsection{Les concepts fondamentaux}
Un autoencodeur est un réseau de neurones composé d'au moins trois couches. Il est dit \anglicisme{feedforward}, ce qui signifie que les connexions entre les neurones ne forment pas de cycle.
Un autoencodeur se décompose en deux parties, l'encodeur et le décodeur comme présentés dans la Figure~\ref{chap:sae:fig:AE}.

\begin{figure}[htp]
\begin{center}
\includegraphics[scale=0.65]{chapters/Graphs/AE_fr}
 \caption{Schéma d'un autoencodeur. Les biais sont ignorés pour simplifier le schéma.}
\label{chap:sae:fig:AE}
\end{center}
%\vspace{-3mm}
\end{figure}

Soit $x$ le vecteur de taille $n$ utilisé en entrée du réseau et $h$ la couche cachée composée de $m$ neurones. L'encodage de $x$ dans $h$ est réalisé de la façon suivante :
\begin{eqnarray}
\label{chap:sae:eq:hidden}
h=\sigma(W^{(1)} x+b^{(1)})\ ,
\end{eqnarray}
Où $W^{(1)}$ est une matrice de poids de taille $n\times m$ et $b^{(1)}$ est un vecteur de dimension $m$. $\sigma(.)$ est la fonction d'activation de l'encodeur. Le \anglicisme{framework} des autoencodeurs permet l'utilisation de plusieurs fonctions d'activation comme celle introduite dans le Chapitre~\ref{chap:nnc}. Dans ces travaux, la fonction d'activation utilisée est la tangente hyperbolique~(\anglicisme{Tanh}) définie ainsi :

%where $W^{1}$ is a $m\times n$ weight matrix and $b^{(1)}$ is a $m$-dimensional bias vector. $\sigma(.)$ is a non-linear activation function. In the approach proposed in this paper the function is the hyperbolic tangent defined as follows:
\begin{equation}
\label{chap:sae:eq:sigma}
\sigma(y)=\frac{e^y-e^{-y}}{e^y+e^{-y}}
\end{equation}

Le décodeur utilise la représentation cachée de $h$ pour reconstruire le vecteur d'entrée $x$ et génère le vecteur de sortie $\tilde{x}$ :

\begin{equation}
\label{chap:sae:eq:output}
\tilde{x}=\sigma(W^{(2)} h+b^{(2)})\ ,
\end{equation}
Ici le vecteur reconstruit $\tilde{x}$
possède $n$ dimensions,
$W^{(2)}$ est une matrice de poids de dimension
$m\times n$ et  $b^{(2)}$
et un vecteur de biais aussi de taille $n$ .

L'estimation des paramètres de l'autoencodeur $\theta=\{W^{(1)},b^{(1)},W^{(2)},b^{(2)}\}$
est réalisée en minimisant l'erreur de reconstruction quadratique moyenne.
$ L_{\textrm{MSE}} $ ~\cite{bengio2009learning} calculée par  :

\begin{equation}
L_{\textrm{MSE}}(\theta)=\frac{1}{d}\sum\limits_{x\in D}l_{\textrm{MSE}}(x,\tilde{x})=\frac{1}{d}\sum\limits_{x\in D}||x-\tilde{x}||^2
\label{chap:sae:eq:LMSE}
\end{equation}
L'adaptation des paramètres $\theta$ est réalisée en utilisant un algorithme de descente de gradient présenté dans le Chapitre~\ref{chap:nnc}.


\subsection{Autoencodeur débruitant (DAE)}
\label{chap:sae:sec:DAE}

Les autoencodeurs classiques compressent l'information présente dans les données d'entrée. Si ces données sont bruitées, celui-ci va affecter de la même manière les représentations compressées. Ce bruit peut dégrader les performances du système. De manière à rendre plus robustes les représentations apprises, le système retient la structure sous-jacente des données~\cite{bengio2009learning}.
Dans ce but, \cite{vincent2010stacked} proposent des réseaux de neurones dit DAE, dans lesquels, le vecteur d'entrée est corrompu artificiellement avant de l'encoder dans $h$. Ensuite, le vecteur $h$ est utilisé pour décoder la version propre de l'entrée $x$. De cette manière, le DAE apprend la distribution des données et du bruit, pour reconstruire la représentation propre et ainsi devenir robuste aux bruits.

\begin{figure}[!htbp]
\begin{center}
\includegraphics[scale=0.55]{chapters/Graphs/pure_dae_fr2}
 \caption {Modèle graphique d'un autoencodeur débruitant. Le vecteur d'entrée $x$ est corrompu aléatoirement pour générer $x^{(\textrm{corrompu})}$. Ensuite $x^{(\textrm{corrompu})}$ est projeté dans l'espace latent $h$ pour ensuite produire la reconstruction $\tilde{x}$. L'erreur de reconstruction est calculée avec $L_{\textrm{MSE}}$.}
 \label{chap:sae:fig:DAE}
\end{center}
%\vspace{-3mm}
\end{figure}

La Figure~\ref{chap:sae:fig:DAE} montre l'architecture d'un DAE. Dans ce modèle, le vecteur d'entrée $x$ est considéré comme ``propre''. L'objectif de ce DAE est d'obtenir, à partir d'un vecteur d'entrée propre, un modèle de reconstruction robuste.
C'est pourquoi $x$ est artificiellement corrompu via une fonction qui peut-être :
% La list est faite de cette manière car la list normal \begin{list} ou \begin{description} ne semble pas marcher.
\begin{itemize}
	\item Un { bruit Gaussien Isotropique Additif \it} (additive gaussian noise, GN) $x^{(\textrm{corrompu})}|x\sim  \mathcal{N}\left( x,\sigma^2 I  \right)$ .
	\item Un {\it bruit par masque} (masking noise, MN), qui force une fraction des éléments de $x$ à $0$.
	\item Un {\it bruit sel-et-poivre} (salt-and-pepper noise, SN), qui force la valeur de certains éléments de $x$ à leur valeur maximum ou minimum selon un tirage aléatoire~\cite{vincent2010stacked}.
\end{itemize}
La projection de $x^{(\textrm{corrompu})}$ dans la couche cachée se calcule comme ci-dessous :
\begin{equation}
h=f_{(W^{(1)},b^{(1)})}(x^{(\textrm{corrompu})})=\sigma(W^{(1)}x^{(\textrm{corrompu})}+b^{(1)})
\end{equation}
et le vecteur reconstruit $\tilde{x}$ s'obtient de manière similaire:
\begin{equation}
\tilde{x}=g_{(W^{(2)},b^{(2)})}(h)=\sigma(W^{(2)} h+b^{(2)})
\end{equation}
Le processus d'apprentissage du réseau détermine les paramètres $\theta=\{W^{(1)},b^{(1)},W^{(2)},b^{(2)}\}$ qui minimisent l'erreur de reconstruction $L_{\textrm{MSE}}(x,\tilde{x})$.

% DAAE ...
%\vspace{-2mm}

%L'utilisation de ce type d'autoencodeur est intéressante puisque qu'ils sont capables de produire une bonne représentation de $h$ à partir d'une représentation partiellement détruite ou corrompue. La représentation latente ainsi construite conserve le maximum de l'informativité de $x$ tout en étant robuste aux variabilités inutiles des distributions d'entrées.

Dans le cadre de nos travaux, le problème peut être formulé différemment. Les caractéristiques d'entrée utilisées sont issues d'un SRAP.
Les transcriptions automatiques contiennent  en particulier des erreurs de transcription et des disfluences qui peuvent gêner la compréhension d'un segment audio. On peut donc considérer les transcriptions comme bruitées.
Ce bruit produit est complexe et imprévisible. %Il est difficile de modéliser ce bruit. Il n'est aujourd'hui pas possible de reconstruire un document propre à partir de la version bruitée produite par SRAP. Bien entendu si cette opération était possible, elle ferait partie intégrante du SRAP.
Ce constat a motivé une approche originale qui utilise un autoencodeur débruitant dont les données corrompues d'entrée et les données propres de sortie ne sont pas issues de la même source (hétérogènes). Celui-ci exploite à la fois la version du SRAP (ASR) et la version transcrite manuellement (TRS) de chaque document. Il ignore le bruit naturellement présent dans les documents transcrits automatiquement pour apprendre une représentation robuste. Le modèle graphique de ce réseau est présenté dans la Figure \ref{chap:sae:fig:modeldae}. On peut remarquer dans cette Figure que les données corrompues ASR sont encodées dans la couche cachée $h$, utilisée à son tour pour construire une version du document propre $\tilde x$. Cette vision est comparée au document transcrit manuellement qui est considéré comme propre.
\begin{figure}[!t]
\begin{center}
\includegraphics[scale=0.55]{chapters/Graphs/DAE_ASR_TRS2}
 \caption{Modèle graphique d'un autoencodeur débruitant hétérogène. Le vecteur d'entrée $x^{(ASR)}$ est corrompu par le SRAP. La sortie $x^{(TRS)}$ provient de transcriptions manuelles.
 }
 \label{chap:sae:fig:modeldae}
\end{center}
%\vspace{-3mm}
\end{figure}


\subsection{Autoencodeur empilé (SAE)}
\label{chap:sae:sec:SAE}
Les réseaux de neurones profonds sont capables de construire des représentations de plus en plus abstraites à l'aide de leurs multiples couches cachées. C'est une des raisons principales à leurs excellentes performances dans de nombreuses tâches~\cite{bengio2007scaling,hinton2006fast}. Dans l'optique d'utiliser cette capacité d'abstraction, des SAE sont utilisés. Ils sont composés d'un nombre variable de couches cachées $k$. Les représentations latentes de la $i$-ème couche cachée $h^{(i)}$, pour une entrée $x$ donnée sont calculées à partir du vecteur $h^{(i-1)}$  comme suit :
%It has been argued in~\cite{bengio2007scaling,hinton2006fast} that DNNs may encode input data at progressively deeper levels of abstraction in successive hidden layers of stacked autoencoders (SAE). In this type of DNN with $k$ hidden layers, the latent features at the $i$-th intermediate hidden layer, for an input vector $x$, are computed as the elements of a vector $h^{(i)}$ as follows:
\begin{equation}
h^{(i)}=\sigma(W^{(i)}h^{(i-1)}+b^{(i)})\  {\forall i \in \{1,\dots,k \} }
\end{equation}
et \begin{equation}
h^{(0)}=x
\end{equation}
\begin{figure}[!t]
\begin{center}
\includegraphics[scale=0.65]{chapters/Graphs/SAE_fr}
 \caption{Architecture d'un autoencodeur empilé (SAE). Les autoencodeurs simples pour le préentrainement sont à gauche. Leurs poids sont utilisés pour initialiser le réseau complet (à droite).
 }
 \label{chap:sae:fig:deepsae}
\end{center}
%\vspace{-3mm}
\end{figure}

La méthode d'apprentissage du SAE se déroule en deux étapes.
D'abord, les poids de chaque couche sont préentrainés indépendamment par un autoencodeur simple qui utilise la projection de la couche précédente. La représentation calculée de chacun des documents projetés dans la couche $h^{(i)}$ est conservée et servira pour calculer  $h^{(i+1)}$ comme montré dans la Figure~\ref{chap:sae:fig:deepsae} (à gauche). Chaque couche cachée supplémentaire apprend ainsi une représentation plus robuste que la précédente.
Ensuite, un réapprentissage global (\anglicisme{fine tuning}) est effectué sur l'ensemble des couches empilées entier (cf. Figure~\ref{chap:sae:fig:deepsae} à droite). Cette étape affine les poids du réseau en entrainant la pile entière à reconstruire le vecteur $x$ d'origine.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXPERIMENTAL PROTOCOL ... %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





\section{Protocole expérimental}
\label{chap:sae:sec:experimentalProtocol}

Cette section présente d'abord les différents autoencodeurs implémentés et les méta paramètres de chacun d'eux dans ces expériences.
Les représentations latentes produites par ces réseaux sont évaluées sur la tâche TID présentée dans le Chapitre~\ref{chap:decoda}.


%\subsection{Méta paramètres des réseaux de neurones}
\label{chap:sae:subsec:flp}

En premier lieu, trois autoencodeurs débruitants simples (non profonds) sont entrainés pour évaluer les différentes compositions de sources de données et pour valider l'apport des modèles profonds.
\begin{enumerate}
	\item \textbf{$\textrm{AE}_{\textrm{ASR}}$ : } utilise en entrée les données $x^{(\textrm{ASR})}$ issues du SRAP avec une corruption artificielle supplémentaire. Il reconstruit le vecteur $x^{(\textrm{ASR})}$.

	\item \textbf{$\textrm{AE}_{\textrm{TRS}}$ : } est un miroir de $\textrm{AE}_{\textrm{ASR}}$, mais en utilisant les données transcrites manuellement $x^{(\textrm{TRS})}$.

	\item \textbf{DAE : } repose sur le principe que les documents produits par le SRAP sont une version bruitée des documents transcrits manuellement. Il utilise en caractéristiques d'entrée le vecteur $x^{(\textrm{ASR})}$ et tente de reconstruire le vecteur $x^{(\textrm{TRS})}$ comme présenté dans la Figure~\ref{chap:sae:fig:modeldae}.
\end{enumerate}


Les couches cachées de ces autoencodeurs respectivement $h^{(\textrm{ASR})}$, $h^{(\textrm{TRS})}$ et $h^{(\textrm{DAE})}$ sont composées de $50$ neurones chacune. Cette taille a été déterminée en fonction des performances des autoencodeurs sur les données de développement de la tâche.
Ensuite, trois autoencodeurs profonds supplémentaires sont entrainés pour ces expérimentations.\\
Ces autoencodeurs devraient avoir les meilleures capacités de modélisation, en construisant des représentations plus abstraites dans les couches plus profondes du réseau. 
%Il est aussi nécessaire de comparer des architectures de dimension similaire pour une comparaison juste de la solution proposée à base de SAE.

\begin{enumerate}
	\item \textbf{DAE profond (DDAE) : } Une évolution plus profonde du DAE. Il possède plusieurs couches cachées, $x^{(\textrm{ASR})}$ est utilisé en entrée pour reconstruire $x^{(\textrm{TRS})}$. Ce réseau est présenté dans la Figure~\ref{chap:sae:fig:ddae}.
	\item \textbf{$\textrm{SAE}_{\textrm{ASR}}$ : } est un autoencodeur empilé qui corrompt artificiellement les vecteurs d'entrée $x^{(\textrm{ASR})}$ par masquage. Il reconstruit le vecteur $x^{(\textrm{ASR})}$.
	\item \textbf{$\textrm{SAE}_{\textrm{TRS}}$ : } est similaire à $\textrm{SAE}_{\textrm{ASR}}$ mais utilise les transcriptions manuelles  $x^{(\textrm{TRS})}$.
\end{enumerate}


\begin{figure}[!ht]
\begin{center}
\includegraphics[scale=0.65]{chapters/Graphs/srap_dae_nn}
 \caption{Architecture d'autoencodeurs débruitants profonds hétérogènes. Ses vecteurs d'entrée sont naturellement bruités par un SRAP et ses vecteurs de sortie propres sont les transcriptions manuelles.
 }
 \label{chap:sae:fig:ddae}
\end{center}
%\vspace{-3mm}
\end{figure}

Les couches cachées de ces réseaux profonds sont de taille $50$, $300$ et $50$.
Tous ces réseaux de neurones reposent sur la même procédure d'apprentissage. Ils utilisent une descente de gradient \cleaned{\anglicisme{Adam}}, un taux d'apprentissage initial de  $10^{-3}$, du \anglicisme{early stopping} (où arrêt prématuré) pour arrêter l'apprentissage à convergence avec une patience de 20 itérations.

Les représentations produites par les différents autoencodeurs sont évaluées en classification sur la tâche TID détaillée dans le Chapitre~\ref{chap:decoda}. Le classifieur utilisé est le MLP présenté dans le Chapitre~\ref{chap:decoda:sec:mlp}.\\

Tous ces réseaux de neurones sont développés avec les bibliothèques Keras~\cite{chollet2015keras} et Theano~\cite{bergstra+al:2010-scipy} pour la manipulation des tenseurs et de l'accélération GPU. Les calculs matriciels sont \cleaned{réalisés par une carte graphique\footnote{Nvidia Geforce TITAN X} avec 3500 coeurs, 12 GB de mémoire.}
Les temps nécessaires pour l'apprentissage de ces différents réseaux sont reportés dans le Tableau~\ref{chap:sae:table:processingTime} et seront commentés dans la Section \ref{chap:sae:sec:conclusion}.

\begin{table}
\centering
\scalebox{1}{
\begin{tabular}{c|c|c|c|c}
{\bf AE} & {DAE \bf } &{\bf DDAE} & {\bf SAE} & {MLP \bf}\\
\hline
10 (min.)  & 10 (min.) & 25 (min.) & 50 (min.) & 8-15 (min)
\end{tabular}
}
\caption{\label{chap:sae:table:processingTime}Temps d'apprentissage pour les différents réseaux de neurones réalisés avec une carte graphique (GPU) Nvidia Geforce TITAN X.}
\end{table}

\section{Résultats et discussion}
\label{chap:sae:sec:results}
Les résultats expérimentaux sur la tâche TID  avec les représentations apprises par les autoencodeurs débruitants simples sont détaillés et commentés dans la section \ref{chap:sae:subsec:AEResults}. Ensuite, la section \ref{chap:sae:subsec:bdae} présente les résultats obtenus avec les autoencodeurs profonds.

\subsection{Analyse des autoencodeurs simples}
\label{chap:sae:subsec:AEResults}
Le Tableau~\ref{chap:sae:table:AE} présente les précisions obtenues par le MLP en utilisant les caractéristiques produites par les différents modèles non profonds.

Parmi ces résultats, les meilleures précisions sont obtenues avec les représentations latentes issues des AE qui utilisent les mêmes données en entrée et en sortie. Ils apportent une amélioration de $3,9$\% par rapport aux représentations TF.IDF avec les données ASR et $0,7$\% avec les données TRS.\\
La forte amélioration obtenue avec $\textrm{AE}_{\textrm{ASR}}$ montre que lorsqu'une information non pertinente est présente, l'autoencodeur arrive à l'ignorer. L'amélioration en précision plus réduite observée par le $\textrm{AE}_{\textrm{TRS}}$ met en évidence que moins d'informations non pertinentes sont présentes avec les données TRS.
%On peut déduire que la variabilité modélisée et supprimée par $\textrm{AE}_{\textrm{ASR}}$ est moins ou pas présente dans les données issues des transcriptions manuelles.
Cela confirme l'hypothèse que les erreurs introduites par le SRAP peuvent s'apparenter à du bruit pour un DAE et qu'il est possible de le supprimer au moins en partie. \\
La précision obtenue avec les représentations du DAE hétérogène (ASR $\rightarrow$ TRS) est de $74,3$\% soit une précision d'environ $3\%$ inférieure aux représentations TF.IDF.GINI de départ. Cette dégradation des résultats montre que malgré les bons résultats des autoencodeurs débruitants, ceux-ci ne sont pas capables de débruiter les erreurs introduites par le SRAP. \cleaned{Les représentations $\textrm{x}^{(\textrm{ASR})}$ et $\textrm{x}^{(\textrm{TRS})}$ sont probablement si différents que le système de débruitage ne parvient pas à extraire une structure commune.} \\
La section suivante aborde les résultats des réseaux de neurones profonds, dont les capacités de modélisation supplémentaire doivent faciliter le débruitage et améliorer les représentations latentes.
%Table~\ref{chap:sae:table:AE} presents theme classification accuracies with features from autoencoders obtained with two different transcription conditions (manual TRS as in Figure \ref{chap:sae:fig:TDAE}a or automatic ASR as in Figure~\ref{chap:sae:fig:TDAE}b ).

 %It is worth emphasizing first that the best accuracies observed are obtained with hidden vectors for homogeneous conditions (ASR $\rightarrow$ ASR and TRS $\rightarrow$ TRS) with a gain of $3.9$ and $0.7$ points for ASR and TRS respectively. Accuracies obtained with the denoising autoencoder DAE from ASR $\rightarrow$ TRS decrease when the vector representation becomes more and more abstract ($\textrm{Acc.}(\tilde{x})<\textrm{Acc.}(h)<\textrm{Acc.}(x)$). As expected, we can note that errors contained in the automatic transcriptions of noisy documents lower the accuracy, from { $77.1\%$ to $83.4\%$
%for the vector $x$ for example}. The accuracy of $84.1\%$ obtained with the autoencoder $\textrm{AE}_{TRS}$ is the best classification result obtained with manual transcriptions. It represents the upper bound of the classification accuracy that can be obtained by denoising the ASR transcriptions (target of the approach proposed in this paper i.e somehow equivalent to an oracle system).

% AE ...
\begin{table}[!ht]
\centering

\scalebox{1}{
\begin{tabular}{c|c|c||c|c|c}

{\bf Méthodes} & {\bf Données } & {\bf Données } & \multicolumn{3}{c}{{\bf Précisions du système}}\\
 & {\bf d'entrée } & {\bf de sortie} &{\bf TF.IDF}& couche cachée $h$ & Reconstuction $\tilde{x}$ \\
\hline
{\bf \color{SkyBlue} $\textrm{AE}_{\textrm{ASR}}$ } & ASR & ASR & 77,1 & {\bf \color{SkyBlue} 81} & 79\\
\hline
$\textrm{AE}_{\textrm{TRS}}$ & TRS & TRS & 83,4 & 84,1  & 83,7\\
\hline
DAE & ASR & TRS & 77,1 & 74,3  & 70,3\\
\end{tabular}
}
\caption{\label{chap:sae:table:AE}Précision(\%) sur la tâche TID en utilisant les représentations produites par les différentes couches cachées des autoencodeurs simples débruitants.}
\end{table}

\subsection{Analyse des autoencodeurs profonds}
\label{chap:sae:subsec:bdae}

Le Tableau~\ref{chap:sae:table:AE_FIXED} compare les performances en classification des caractéristiques obtenues avec les autoencodeurs profonds. Les résultats utilisant le vecteur d'entrée $x$ ne sont pas répétés dans ce tableau puisqu'ils sont identiques à la colonne TF.IDF présentée dans le Tableau \ref{chap:sae:table:AE}.

% DAAE ...
\begin{table}[!ht]
\centering

%\scalebox{1}{
\begin{tabular}{c|c|c|c||c}

{\bf Autoencodeur} &  {\bf Type } & {\bf Type} & {\bf Couche} & {\bf Précision}\\
{\bf employé} & {\bf d'entrée  } & {\bf de sortie } & {\bf utilisée} & {\bf obtenue}\\

\hline
% & ASR & -- & $x$  & 77,1\\
 & ASR & ASR & $h^{(1)}$  & 81,7\\
{\color{SkyBlue} \bf $\textrm{SAE}_{\textrm{ASR}}$ } & ASR & ASR & $h^{(2)}$  & { \color{SkyBlue} \bf 82,0 }\\
 & ASR & ASR & $h^{(3)}$  & 80,1\\
 & ASR & ASR & $h^{(4)}$  & 81,0\\

\hline
  & TRS  & TRS & $h^{(1)}$  & 84,1\\
 $\textrm{SAE}_{\textrm{TRS}}$ & TRS & TRS & $h^{(2)}$  & 84,7\\
  & TRS & TRS & $h^{(3)}$  & 85,3\\
 & TRS & TRS & $h^{(4)}$  & 84,4\\
\hline
  & ASR & TRS & $h^{(1)}$ & 72,5 \\
%\hline
DDAE & ASR & TRS & $h^{(2)}$ & 70\\
%\hline
 & ASR & TRS & $h^{(3)}$ & 69,4\\
%\hline
 & ASR & TRS & $\tilde{x}$ & 69,7\\

%Deep Adaptive AE & $\W_1$ and $\W_2$ & $H_{\textrm{ASR}}$  & 80,7\\
%\hline
%Proposed {\bf SDAE} & ASR & TRS & $h$ &{ 83,2}\\
%\hline
%Deep Adaptive AE & $\W_1$ and $\W_2$  & $H_{\textrm{TRS}}$  & 82,6\\
%\hline
%Deep Adaptive AE & $\W_1$ and $\W_2$  & Output  & 82,3\\

\end{tabular}
%}
\caption{\label{chap:sae:table:AE_FIXED}Précision (\%) de classification thématique en utilisant les caractéristiques produites par les différentes couches des autoencodeurs profonds.}
\end{table}

Ce tableau met en évidence que les meilleurs résultats sont obtenus par les réseaux homogènes. En effet le $\textrm{SAE}_{\textrm{ASR}}$ obtient $82$\% soit $4,9$ point d'amélioration absolue et surpasse toutes les autres configurations utilisant les données bruitées issues du SRAP.\\
Le $\textrm{SAE}_{\textrm{TRS}}$ permet de gagner $2,2$\% comparé aux données d'origine. Lorsque les données d'entrée et de sortie sont issues d'un environnement similaire, un autoencodeur profond arrive à découvrir une forme de structure dans les données, pour construire une représentation moins sensible aux bruits, avec un effet positif sur les capacités de généralisation du modèle.
Au contraire du DDAE, qui voit l'ensemble de ses couches proposer des représentations latentes de piètre qualité. Il n'obtient que $72,5$\% de précision dans le meilleur des cas, soit plus de $4$\ points en dessous du système de référence. Les capacités d'abstraction supplémentaires apportées par les couches cachées ne changent pas l'incapacité du réseau à débruiter les représentations issues du SRAP. Le bruit introduit par la transcription automatique est trop imprévisible. Il apporte une variabilité trop importante pour modéliser une structure commune aux représentations issues des sources hétérogènes (manuelle et automatique).


Les autoencodeurs empilés améliorent de façon notable la qualité des représentations pour la classification.
Par contre, on remarque qu'augmenter le niveau d'abstraction de la représentation des données n'est pas forcément le facteur qui permet aux réseaux de neurones d'améliorer la qualité des représentations. En effet, les dernières couches ($h^4$) produisent des représentations de moins bonnes qualités que les précédentes. Avoir plus d'une couche cachée est bénéfique pour le système global, mais la robustesse aux bruits joue aussi un rôle décisif dans les gains apportés pour cette tâche.
Les performances en classification des documents parlés se voient améliorées par l'utilisation des SAE, à la fois pour les transcriptions manuelles et les transcriptions automatiques. L'écart entre les performances obtenues avec les documents issus du SRAP et les documents transcrits manuellement, lui se réduit. En effet, dans leurs versions sans encodage, le bruit introduit une erreur de classification de $6,4$\%. Après encodage par les SAE, la différence n'est plus que de $3,3$\%. On peut en déduire qu'une partie de la variabilité inutile introduite par le SRAP est effectivement modélisée et supprimée de façon non supervisée.



% \subsection{Discussion}
% \label{chap:sae:subsec:discusion}

% %ALL RESULTS ...
% \begin{table}[!ht]
% \centering
% \scalebox{1}{
% \begin{tabular}{c|c||c}

% {\bf Method} & {\bf Feature} & {\bf Test}\\
% {\bf employed} & {\bf vector} & {\bf Accuracy}\\
% \hline
% %\hline
% %\hline
% TF.IDF & -- & 77.1 \\
% %\hline
% $\textrm{AE}_{\textrm{ASR}}$ & $h$ & 81\\
% SAE & $h^{(2)}$  & 82.0\\
% \hline

% \end{tabular}
% }
% \caption{\label{chap:sae:table:Accuracy_Recap_Decoda}Best theme classification accuracy (\%) observed for each set of features from ASR.}
% \end{table}

% Table \ref{chap:sae:table:Accuracy_Recap_Decoda} compares the best accuracies of the different architectures and features proposed in this paper. Although  $\textrm{AE}_{\textrm{ASR}}$ and SAE are simpler compared to others, their parameters are computed with a completely unsupervised learning.
% They are among the more robust evaluated approaches. This can be explained by the fact that these neural networks are both able to remove an important portion of the {  erratic noise contained in the automatically transcribed documents. }
% %The little gap between them could be explained by the small amount of data and the already process and relatively small vocabulary of the features set used for the task.
% However, both methods can not achieve the performance obtained with the original clean corpus.
% This means that there is still an other kind of noise introduced by the automatic transcription step that can not be compensated with
% noisy examples only.
% The noise removed by the $\textrm{AE}_{\textrm{ASR}}$ method is not only due to the automatic transcription as pointed out in
% Table~\ref{chap:sae:table:Accuracy_Recap_Decoda} {   by the $\textrm{AE}_{\textrm{TRS}}$ still achieving a relative improvement of only 0.83\% compared to the 3.76\% for the $\textrm{AE}_{\textrm{ASR}}$.} %This confirms that the noise can be strip in two parts, the residual noise and the noise introduced by the automatic speech recognition step.

\section{Conclusion}
\label{chap:sae:sec:conclusion}

%Secondly, the mapping layer is a good representations for thematic classification, but the BDAE is far from able to produce a good human readable representations of documents.
Ce chapitre présente plusieurs représentations latentes de transcriptions de documents parlés construites à partir de réseaux de neurones.
Parmi les réseaux de neurones utilisés,
les autoencodeurs (AE) qui utilisent des caractéristiques homogènes permettent une amélioration intéressante de la qualité des représentations. En effet, ces réseaux modélisent la structure des données employées et génèrent une représentation robuste aux variabilités non pertinentes. Les meilleures performances sont obtenues par les SAE qui permettent, en projetant les documents dans plusieurs couches cachées, de créer une modélisation des données plus abstraite. Celle-ci réduit l'effet relatif des erreurs du SRAP et facilite la classification des documents, ce qui permet au classifieur de généraliser plus facilement à de nouvelles données.

En revanche, les autoencodeurs hétérogènes n'arrivent pas à modéliser de relations utiles entre les données bruitées et les données propres. Les erreurs contenues dans les documents ASR produisent des représentations qui sont trop différentes des données TRS. Les (D)DAE ne parviennent pas à modéliser une variabilité utile de ces différences.
\\
Les temps d'apprentissage reportés dans le Tableau~\ref{chap:sae:table:processingTime} montrent que seule l'architecture joue un rôle sur le temps d'apprentissage. Les AE et le DAE nécessitent le même temps d'apprentissage alors qu'ils ne reposent pas sur des données identiques et pourraient ne pas converger vers une solution à la même vitesse.
On remarque aussi que le SAE nécessite cinq fois plus de temps d'entrainement que l'$AE_{ASR}$ à cause des préentrainements, alors qu'il n'apporte qu'un gain de 1\%. Si le temps d'apprentissage est un critère important, se contenter d'un AE simple peut être une solution viable. \\
Ces travaux sont ouverts à plusieurs axes d'amélioration pour continuer à réduire l'impact du bruit introduit par le SRAP et combler la différence de performance observée avec les transcriptions manuelles. \\
Un premier axe serait de trouver une méthode plus robuste que le DDAE, capable d'exploiter l'information des transcriptions manuelles, dans le but d'améliorer la transcription automatique. Cet axe sera abordé dans les Chapitres  \ref{chap:sdae} et \ref{chap:tdae}. \\
Le second axe serait d'appliquer le processus présenté dans ce chapitre, sur d'autres caractéristiques de description textuelle (c.f. Chapitre \ref{chap:docs}), issues de documents transcrits automatiquement.  %ou des réseaux de neurones récurrent par exemple permettent de créer des représentation du texte différentes qui pourrais profité de ces travaux. %[ c'est l'axe qui est abordé dans le chapitre suivant.]
