\chapter{Problématique et cadre expérimental}
\label{chap:decoda}
\minitoc 

\section{Introduction}
\label{chap:decoda:sec:decodaCorpus}

\cleaned{Le signal audio est porteur de types d'informations variés, ce qui rend son interprétation complexe pour un système automatique. Pour résoudre un problème de compréhension du langage (SLU), un système automatique doit interpréter les informations transmises par la parole. L'extraction de ces informations parlées est le plus souvent réalisée par un système de reconnaissance automatique de la parole (SRAP). Un SRAP produit des hypothèses de transcription dissociées des autres informations acoustiques. Les résultats de transcription sont bien souvent imparfaits. Ces imperfections s'expliquent par la présence de dysfluences ou de conditions acoustiques sous-optimales. Les transcriptions erronées introduisent des risques d'erreurs de compréhension. Les performances d'un système de SLU sont donc fortement dépendantes des résultats de transcriptions automatiques. Les contributions de ce manuscrit proposent d'exploiter des représentations neuronales de transcriptions pour améliorer les performances des systèmes de SLU.
Dans ce chapitre, nous présentons, dans un premier temps, une tâche d'identification de thématique (TI) qui sera utilisée, tout au long du manuscrit, pour évaluer des systèmes automatiques. Dans un second temps, des systèmes de référence sont introduits.}

\section{Tâche d'Identification de Thématique (TI)}
La tâche d'identification de thématique a été introduite dans la Section~\ref{chap:init:subsec:iterHH} consiste à déterminer la thématique principale d'un segment audio parmi un ensemble prédéterminé de thématiques. Dans ces travaux, nous utilisons une tâche de TI construite à partir des données du projet DECODA\footnote{http://decoda.univ-avignon.fr/}. Pour simplification nous appellerons cette tâche <<TID>>. \\
Les données utilisées dans TID sont constituées d'un ensemble de conversations téléphoniques issues de la centrale d'appel de la Régie Autonome des Transports Parisiens (RATP), qui est en charge des transports publics dans la ville de Paris. Ces conversations ont deux acteurs : un agent RATP (humain) et un client. Le corpus est composé de 1 242 conversations soit environ 74 heures de signal de parole. 
%Le corpus est découpé en trois morceaux, ces morceaux sont nécessaire pour l' évaluation de système automatique. Le découpage est rapporté dans les colonnes du Tableau~\ref{chap:decoda:table:Decoda_Dataset}. 
L'ensemble des conversations du corpus est annoté et transcrit manuellement. Les annotations manuelles comportent des informations thématiques et syntaxiques. Les détails sur les annotations sont accessibles dans~\cite{bechet2012decoda}. Dans la suite de ce document, nous manipulerons deux types de transcription, les transcriptions manuelles notées <<TRS>> et les transcriptions produites par un système automatique notées <<ASR>>.\\
Dans le cadre expérimental étudié, nous nous intéresserons particulièrement aux informations thématiques. Chaque conversation a été manuellement annotée par un agent humain parmi les 8 labels suivants:
\begin{enumerate}
    \item Problème d'itinéraire
    \item Objets trouvés
    \item Horaires
    \item Carte de transport
    \item État du trafic
    \item Prix du ticket
    \item Infractions
    \item Offres spéciales
\end{enumerate} 
L'hypothèse qu'une conversation puisse appartenir à une unique classe est discutée dans \cite{hazen2011topic}. Cette hypothèse peut être acceptable si la classification correspond à la thématique principale abordée dans une conversation. Par exemple, le schéma~\ref{chap:decoda:fig:multi_theme_dialogue} montre une conversation entre un agent et un client où le thème principal est un problème d'itinéraire. Chaque conversation a été associée à un thème principal, des thèmes secondaires sont également présents, mais ils ne sont pas considérés dans le cadre de ce travail.
\begin{figure}[htp]
\begin{center}
\includegraphics[scale=1]{chapters/2-unidim/Graphs/convers}
 \caption{Exemple de dialogue, transcrit manuellement, extrait de la tâche TID attribué à la catégorie ``problème d'itinéraire'' par un expert, mais qui contient aussi le thème ``État du trafic''.}
\label{chap:decoda:fig:multi_theme_dialogue}
\end{center}
%\vspace{-3mm}
\end{figure}
\begin{table}[h!]
\centering
\centering
\caption{\label{chap:decoda:table:Decoda_Dataset}Description des classes dans la tâche TID.}
\vspace{1mm}
\scalebox{1.0}{
\begin{tabular}{c c c c}
\hline
{}&\multicolumn{3}{c}{{\it Nombre d' échantillons}} \\
\cline{2-4}
{\bf Classes} & {\bf Entrainement} & {\bf Developpement} & {\bf Test}\\
\hline
Problème d' itinéraire & 145 & 44 & 67 \\

Objets trouvés & 143 & 33 & 63 \\

Horaires & 47 & 7 & 18 \\

Carte de transport & 106 & 24 & 47 \\

État du trafic & 202 & 45 & 90 \\

Prix du ticket & 19 & 9 & 11 \\

Infractions & 47 & 4 & 18 \\ 

Offres spéciales& 31 & 9 & 13 \\
\hline
\hline
{\bf Total} & {\bf 740} & {\bf 175} & {\bf 327} \\
\hline
\end{tabular}
}
\end{table}
La répartition des conversations dans les différentes classes est répertoriée dans le Tableau~\ref{chap:decoda:table:Decoda_Dataset}.\footnote{
L'utilisation des termes, labels et classes dans ce manuscrit fera toujours référence à ces 8 labels. Alors que les termes thème et topic feront référence à ceux appris par les modèles statistiques. c.f. Chapitre~\ref{chap:docs}}\\
La répartition des dialogues dans les différentes classes est très inégale. Certaines classes sont jusqu'à dix fois moins présentes que la plus représentée. On peut remarquer dans ce tableau que le corpus est découpé en trois parties. Ce découpage est réalisé pour la tâche de classification. Le corpus d'entrainement est utilisé pour entrainer les systèmes automatiques. Le corpus de développement est utilisé pour optimiser les méta paramètres (nombre de couches cachées, nombre de neurones, taux d'apprentissage \dots). Le corpus test sert lui à comparer les systèmes proposés. \\
Les données du projet DECODA ont la particularité d'être directement issues d'une centrale d'appel. Les dialogues ne sont donc pas simulés. Elles représentent donc en matière de qualité et de cohérence un cadre applicatif réel. \\
La qualité du signal audio \cleaned{ est très variable et dépend du type de téléphone et du lieu de l'appel. De plus, les discours ne sont ni préparés ni cadrés.}
De nombreux phénomènes propres à la parole spontanée sont présents. On entend parfois des gens en condition de stress ou en colère dont la prononciation n'est plus classique. Le vocabulaire aussi n'est pas contraint, beaucoup parmi les mots employés par les clients sont hors du vocabulaire du SRAP. Leur présence rend les conversations difficiles à transcrire et donc leur interprétation encore plus difficile.

Les classes utilisées pour l'annotation n'ont pas été choisies pour faciliter la séparation sémantique. Elles proviennent de l'ontologie utilisée par la RATP. Cette ontologie est choisie pour répondre à un besoin applicatif. Par conséquent, d'un point de vue sémantique, certaines classes ont une intersection forte. Enfin, la répartition des documents dans les différentes catégories est très inégale. Toutes ces difficultés cumulées rendent les classes difficiles à distinguer les unes des autres pour un système automatique. \\
Attribuer à chaque document une classe thématique revient donc à une tâche de classification utilisant les informations extraites du signal parlé.
L'évaluation de cette tâche se fait par la métrique de précision multiclasse $P$, qui évalue la proportion de labels correctement attribués par le système comme présenté dans l'équation~\eqref{chap:decoda:eq:precision}.
\begin{equation}
P = \frac{\sum\limits_{i=1}^c Dj_i }{\sum\limits_{i=1}^c D_i} \times 100
\label{chap:decoda:eq:precision}
\end{equation}
Où $Dj_i$ représente le nombre de documents correctement classifiés dans la classe $i$ et $D_i$ le nombre de documents dans la classe $i$ avec $c$ le nombre de classes. Elle est exprimée en pourcentage.

\section{Les systèmes de référence}
Nous allons maintenant présenter le processus de labellisation automatique et les systèmes standards qui serviront de référence pour évaluer l'apport de nos contributions. On peut représenter le processus de labellisation par un système en trois étapes comme illustré dans le schéma~\ref{chap:decoda:fig:pg_decoda}.
 
\begin{figure}[!ht]
\begin{center}
\includegraphics[scale=0.7]{chapters/2-unidim/Graphs/Processe_general}
 \caption{Processus simplifié de labellisation de conversation pour la tâche TID.}
 \label{chap:decoda:fig:pg_decoda}
\end{center}
%\vspace{-3mm}
\end{figure}

La première étape correspond à la transcription automatique de la parole. La deuxième étape correspond à l'extraction des descripteurs nécessaires pour la troisième étape, la classification.
\cite{leelee} expliquent \cleaned{que ce processus se résume souvent à l'enchainement du système de} reconnaissance de la parole et du système de recherche d'informations avec des systèmes simples dans l'étape deux.
Les contributions qui seront présentées dans ce manuscrit interviennent dans l'étape d'extraction des descripteurs. En ajoutant de <<l'intelligence>> dans cette étape, nous pouvons créer des représentations de l'information plus adaptées à la classification et moins sensibles aux erreurs introduites par le SRAP.
Ces étapes sont détaillées dans la section suivante.


\subsection*{La reconnaissance automatique de la parole.}
\label{chap:decoda:subsec:ASR}
La première étape de reconnaissance automatique de la parole permet de passer d'une représentation en signal sonore de bas niveau, à une suite d'hypothèses de transcription (mots ordonnés chronologiquement). La complexité des signaux audios rend la segmentation et l'extraction de concept de haut niveau bien plus efficace sur des données textuelles que sur le signal directement~\cite{DeMori2007}. Il est donc nécessaire de recourir à un SRAP pour pouvoir abstraire la variabilité et la complexité du signal. En contrepartie, sur certains types de documents, le SRAP introduit une importante quantité d'erreurs qui rendent sous optimal l'exploitation des prédictions du système.

Dans le cadre des expériences sur le corpus de la tâche TID, la transcription automatique de la parole est réalisée avec le SRAP LIA-Speeral~\cite{linares2007lia}. LIA-Speeral utilise des modèles acoustiques triphone appris par un modèle de mélange de gaussiennes utilisant 230 000 gaussiennes. Les paramètres du modèle sont estimés par <<maximum à postériori>> (MAP) sur 150 heures de parole en condition téléphonique. \\
Le vocabulaire du SRAP contient 5 782 mots. Le modèle de langue (ML) trigramme utilisé est obtenu en interpolant deux modèles. Le premier est un ML générique appris à partir de données textuelles comme Wikipedia, des dépêches AFP et les transcriptions manuelles de campagnes d'évaluation françaises. Le second est appris à partir des transcriptions manuelles issues du corpus d'entrainement de DECODA. 

Un SRAP est évalué au moyen de la métrique du Taux d'erreur mot (word error rate, WER). Le WER évalue le nombre d' erreurs introduites par le système comparé au nombre de mots correctement transcrits. Il est exprimé en pourcentage. %Pour les détails de calcul du WER je vous renvoie à l'annexe~\appendixtodo{Faire une annexe sur le WER}.

Dans ces conditions, le système LIA-Speeral obtient un WER de 33,8\% sur le corpus d'entrainement, 45,2\% sur le développement et 49,5\% sur le test. Ces taux d'erreurs sont fréquents sur ce type de données et représentatifs des problématiques peu contrôlées réelles. Des résultats comparables sont obtenus dans~\cite{garnier2008callsurf}. Dans ces conditions d'évaluation, environ un mot transcrit sur deux est une erreur. C'est une mesure du bruit introduit par le SRAP. Les systèmes qui reposent sur le SRAP vont donc devoir être capables de gérer ces informations manquantes, superflues ou trompeuses. Ces forts taux d'erreurs s'expliquent notamment par les spécificités des locuteurs, des environnements bruités, mais aussi des déformations linguistiques dues à la conversation spontanée (hésitations, reprises, répétitions, erreurs de grammaire, paroles simultanées, \dots). Notons qu'une <<stop list>> de 126 mots outils~\footnote{http://code.google.com/p/stop-words/} (déterminants, articles\etc ) sont supprimés avant le calcul du WER et ne sont plus utilisés ensuite. L'absence de ces mots peut avoir un impact sur les WER présentés ci-dessus, mais nous n'avons pas mesuré cet impact.

Pour faciliter la suite de la chaine de traitement, il serait envisageable de réduire le WER en tentant d'améliorer le SRAP. Cependant, conserver un taux d'erreur mots important est plus représentatif des cas applicatifs réels, où les caractéristiques acoustiques environnementales ne sont pas contrôlées. C'est le cas notamment avec les problématiques de l'entreprise Orkis qui finance ces travaux. Dans leur système informatique, le système d'extraction d'informations de documents parlés est utilisé sur des données envoyées par les utilisateurs de tous niveaux d'expertise. Il est donc évident que le SRAP produira des erreurs sur au moins un sous-ensemble de ces documents.

\subsection*{L'extraction d'informations.}
Le second processus de traitement correspond à l'étape de prétraitement des résultats du SRAP et d' extraction d'informations.
Les caractéristiques utilisées pour représenter un document peuvent être les mots employés, leurs relations dans la phrase ou la structure de la conversation. 
Le choix des caractéristiques pertinentes à produire à partir de documents textuels est un élément déterminant pour tout système d'extraction d'informations.
Ce problème est encore plus complexe dans le cadre de documents parlés, puisque les mots exploités sont les hypothèses issues d'un SRAP avec des erreurs de transcription. 
Malgré ces difficultés, de bons résultats ont été obtenus sur des tâches d'analyse de la parole~\cite{melamed2011speech}, d'identification de thématiques~\cite{lagus-kuusisto:2002:SIGDIAL03,hazen2011topic} et de la segmentation~\cite{eisenstein2008bayesian,purver2011topic}. D'autres tâches d'analyses de conversations parlées sont détaillées dans la Section~\ref{chap:intro:sec:slu}.%\cite{tur2011spoken}. 

L'objectif de cette étape est de retenir et de mettre en forme les informations les plus pertinentes et les plus utiles possible pour déterminer une thématique. Ceci permet de faciliter le travail du classifieur. Dans les systèmes initiaux, deux méthodes différentes sont utilisées.
Les deux méthodes commencent par une étape de filtrage des mots vides, basée sur la <<stop list>> de 126 mots (c.f. Section~\ref{chap:decoda:subsec:ASR}).

La première méthode représente les documents sous forme de sac-de-mots binaires (BBOW, c.f. Chapitre~\ref{chap:docs}) avec un vocabulaire d'environ $7 000$ mots sans les mots outils. Cette représentation génère des vecteurs très larges et principalement creux. Elle est simple à mettre en place, mais donne généralement des résultats limités.

La seconde méthode (TF.IDF) réalise un prétraitement classique pour la recherche d'informations. D'abord un score de discrimination est attribué à chaque mot dans chaque thématique. Ce score repose sur la combinaison TF.IDF.GINI comme décrite dans le Chapitre~\ref{chap:docs:sec:bow}. Ensuite, les $100$ mots les plus discriminants par classe sont sélectionnés et cumulés. Si des mots discriminants sont présents dans plus d'une classe parmi les huit, ils ne sont comptés qu'une fois. Le vocabulaire d'environ $7 000$ mots est donc réduit à un ensemble de $707$ mots discriminants qui sera le vocabulaire conservé pour la classification. Ensuite, pour chaque document est attribué un vecteur sac-de-mots de la taille du vocabulaire discriminant, où chaque mot est lié à son score TF.IDF.GINI. Cette représentation est moins large et plus informative que la précédente, mais elle est toujours principalement creuse.



\subsection*{Les classifieurs}
\label{chap:decoda:sec:mlp}
La troisième étape de classification attribue un label à chacun des documents. Pour la classification nous avons choisi d'utiliser un perceptron multicouche (MLP), présenté dans le Chapitre~\ref{chap:nnc}, pour ses performances générales et sa vitesse d'apprentissage sur GPU. La sélection des bons métaparamètres pour le réseau tels que sa taille, sa profondeur, le type de lot entre autres sont cruciaux pour son efficacité.

Les descripteurs issus du corpus d'entrainement sont utilisés pour entrainer des MLP avec différents métaparamétres. 
Les meilleures caractéristiques du MLP sont déterminées via leurs performances sur la partie développement du corpus.
La performance du système est ensuite évaluée sur le test. Ce principe permet aux scores de précision d'être plus représentatifs des performances et des capacités de généralisation du modèle qui sont particulièrement importantes dans un cadre applicatif pour TID.
La configuration retenue du MLP est décrite dans le Tableau~\ref{chap:decoda:tab:MLP:params}. \\
% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table}[h!]
\centering
\caption{Métaparamètres sélectionnés pour le MLP appliqué sur TID.}
\label{chap:decoda:tab:MLP:params}
\begin{tabular}{l|c|c|c}
\hline
 & Nom & Taille (en neurones)& Activation \\
\hline
\multirow{4}{*}{Architecture} & Entrée & 7 400 (taille du vocabulaire) & - \\
 & C. Cachée 1 & 512 & Tanh \\
 & C.Cachée 2 & 256 & Tanh \\
 & Sortie & 8 (nombre de labels) & Softmax \\
\hline
\hline
\multirow{2}{*}{Générale} & Fonction de cout & Descente de Gradient & \\
\cline{2-3}
 & Entropie Croisée & Adam & \\

\end{tabular}
\end{table}
Les précisions des deux systèmes présentés ci-dessus sont répertoriées dans le Tableau~\ref{chap:decoda:tab:res:init}.
À titre de comparaison, le tableau contient aussi les résultats d'un SVM utilisant des caractéristiques TF.IDF.GINI proches décrites dans \cite{morchid2014improving} ainsi que les résultats d'un système MLP qui utilise un oracle (transcription manuelle) au lieu du SRAP.

\begin{table}[h]
\centering
\caption{Précisions des méthodes simples sur TID et les résultats MLP TRS dans le cas où des transcriptions idéales (manuelles) remplacent le SRAP. }
\label{chap:decoda:tab:res:init}
\begin{tabular}{l|c|c|c|c}

Classifieur & Entrée & Précision & intervalle de confiance (p < 0.05)\\

MLP & BOW - Binaire & 59,0\% & +/- 5\% \\

MLP & BOW - TF.IDF.GINI & { \bf \color{SkyBlue} 77,1\% } & +/-4,7\% \\

SVM & BOW - TF.IDF.GINI & 73,5\% & +/-4,9\%\\ 
\hline
MLP TRS & BOW - TF.IDF.GINI & 83.4\% & +/-3,7\%

\end{tabular}
\end{table}

Il est intéressant de voir dans ces résultats que le MLP qui repose sur le SRAP est au moins 6,3\% moins bon que le système qui repose sur un oracle. Ce qui confirme que les erreurs introduites par le SRAP ont un impact négatif sur les performances globales du système. 
En contrepartie, la chute en performance est relativement faible au regard des 49,5\% de \anglicisme{WER}. On peut en déduire que les mots porteurs de sens sont majoritairement correctement transcrits, ce qui permet aux classifieurs de détecter le plus souvent le bon label. Comme attendu, les caractéristiques plus élaborées TF.IDF.GINI réalisent de meilleurs résultats que des sacs de mots binaires.
Les intervalles de confiances montrent que la taille du corpus de test rend les différences de performances faiblement significatives. Notamment lorsque sont comparé les résultats du classifieur SVM de la littérature et les résultats du classifieur MLP que nous proposons. Pour compenser cet effet les résultats présentés dans la suite du manuscrit correspondent aux meilleurs résultats de plusieurs évaluations et dans le Chapitre~\ref{chap:tdae}.


\section{Conclusion}
Les performances globales obtenues sur cette tâche peuvent être améliorées selon trois axes.

Le premier axe est l'amélioration des performances du SRAP. En effet, la comparaison des performances entre le SRAP et l'oracle montre clairement qu'une meilleure transcription automatique améliorerait les résultats du système. Cet axe est un domaine de recherche en plein essor qui mobilise beaucoup de chercheurs depuis 1930~\cite{Juang2004AutomaticSR} et encore aujourd'hui~\cite{Xiong2016AchievingHP}. Malgré les très bonnes performances obtenues récemment~\cite{Xiong2016AchievingHP}, dans un contexte industriel/applicatif réel il est fréquent de travailler avec des données non maitrisées lexicalement et acoustiquement. Rencontrer des transcriptions de mauvaise qualité est donc inévitable.  Il est nécessaire pour des systèmes de compréhension de la parole de considérer ces situations. 

Le second axe porte sur l'étape de classification en remplaçant le MLP par un classifieur plus efficace ou plus robuste. On pourrait par exemple évaluer les récentes <<Deep Forest>>~\cite{zhou2017deep} ou créer une architecture de réseau de neurones très profonde. Cet axe, bien que fortement susceptible d'améliorer les performances du système, est aussi le plus intensément étudié par la communauté et nécessite plus de ressources pour produire des systèmes de plus en plus complexes~\cite{esteve2015integration,Xiong2016AchievingHP,chan2016listen,Simonyan2014VeryDC,he2016deep} pour une tâche où la quantité des données disponibles est relativement faible et le cout d'annotation important.

La troisième approche possible est de travailler au niveau de l'extraction de caractéristiques. Les réseaux de neurones fournissent un cadre de travail intéressant qui permet de créer de nouvelles représentations des documents pour améliorer les capacités de généralisation du modèle, sélectionner l'information pertinente, essayer de combler la différence entre représentation issue d'un SRAP et représentation transcrite manuellement. C'est cet axe d'amélioration qui est étudié dans ce manuscrit.

Le prochain Chapitre va aborder l'utilisation des caractéristiques vectorielles abstraites des mots produites par des réseaux de neurones \anglicisme{Word2vec}. \cleaned{Ces descripteurs apportent une modélisation sémantique nouvelle et des possibilités de généralisation qui peuvent améliorer l'identification de thématiques et rendre le système moins sensible aux erreurs du SRAP.}

% Info Sup = 

% The second experiment evaluates the proposed weighting approach in a classification task using the DECODA project corpus~\cite{bechet2012decoda} which aims at identifying conversation themes. This corpus is composed of 1,242 telephone conversations split in training, dev. and test validation sets, each conversation being manually annotated with one of the 8 themes as shown in Table~\ref{chap:decoda:table:Decoda_Dataset}.

% \begin{table}
% \begin{center}
% \caption{\label{chap:decoda:table:Decoda_Dataset}Description of the DECODA dataset.}
% %\begin{minipage}{\textwidth}
% %\scalebox{1}{
% \begin{tabular}{c|ccc}
% %\hline
% {\bf Class}&\multicolumn{3}{c}{{\it Number of samples}} \\
% \cline{2-4}
% {\bf label} & {\bf training} & {\bf dev.} & {\bf text}\\
% \hline
% problems of itinerary & 145 & 44 & 67 \\
% \hline
% lost and found & 143 & 33 & 63 \\
% \hline 
% time schedules & 47 & 7 & 18 \\
% \hline 
% transportation cards & 106 & 24 & 47 \\
% \hline 
% state of the traffic & 202 & 45 & 90 \\
% \hline 
% fares & 19 & 9 & 11 \\
% \hline 
% infractions & 47 & 4 & 18 \\ 
% \hline 
% special offers & 31 & 9 & 13 \\
% \hline
% \hline
% {\bf Total} & {\bf 740} & {\bf 175} & {\bf327} \\
% %\hline
% \end{tabular}
% %}
% %\end{minipage}
% \end{center}
% \end{table}


% %A large vocabulary continuous speech recognition (LVCSR) system with 230,000 Gaussians in the triphone acoustic models has been used for the experiments. 
% The LIA-Speeral automatic speech recognition (ASR) system~\cite{linares2007lia} with 230,000 Gaussians in the triphone acoustic models has been used for the experiments. 
% Model parameters were estimated with maximum {\it a posteriori} probability (MAP) adaptation from 150 hours of speech in telephone condition. 
% The vocabulary contains 5,782 words, a 3-gram language model (LM) was obtained by adapting a basic LM with the transcriptions of the DECODA training set. 
% % ``stop list'' of 126 words\footnote{http://code.google.com/p/stop-words/} removed unnecessary words.
% The ASR system obtains Word Error Rates (WER) of 33.8\% on the training, 45.2\% on the development, and 49.5\% on the test set. These high WERs are mainly due to speech disfluencies and to adverse acoustic environments for some dialogues. 
% %(calls from train stations, noisy and crowded streets with mobile phones...).
% %The ASR system used for the experiments is LIA-Speeral~\cite{linares2007lia} which results in a WER of 33.8\% (train), 45.2\% (dev), and 49.5\% (test) mainly due to speech disfluencies and adverse acoustic environments. 