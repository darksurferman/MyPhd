\chapter{Utilisation de caractéristiques supervisées dans un autoencodeur hétérogène.}

\label{chap:tdae}
\minitoc
	
\section{Introduction}
Dans le Chapitre~\ref{chap:sae}, nous avons vu qu'\cleaned{un DAE profond entrainé en considérant directement les données issues d'un SRAP comme bruitées et les données transcrites manuellement comme références ne parvient pas à construire une représentation efficace.}
Dans le chapitre précédent, nous avons proposé une représentation robuste aux erreurs de transcription automatique en exploitant la supervision apportée par les documents transcrits manuellement. Pour cela, un autoencodeur débruitant particulier a été introduit, le SDAE. Celui-ci crée un espace de projection intermédiaire en apprenant à reconstruire un document transcrit manuellement à partir des transcriptions automatiques.
L'entrainement du SDAE apprend à lier deux représentations latentes apprises de manière non supervisée. La première étant obtenue à l'aide de transcriptions automatiques et la seconde grâce à celles manuelles.

Les résultats expérimentaux présentés dans la Section~\ref{chap:sdae:sec:res} ont montré qu'il est plus facile de retrouver l'information latente d'un document propre que de le reconstruire sous sa forme <<visible>>. La construction des représentations latentes de manière non supervisée permet de construire un vecteur qui contient toute l'information nécessaire pour reconstruire le \cleaned{ document d'origine. Cette particularité favorise la généralisation du système. En contrepartie, l'excès de généralisation sur l'information favorise le risque que le système conserve plus d'informations que nécessaire pour la tâche et donc plus de bruits aussi. Cela rend la représentation plus difficile à nettoyer.}

Nous proposons dans ce chapitre d'introduire de la supervision plus tôt dans le processus de débruitage des documents transcrits automatiquement. Celle-ci est employée pour l'extraction de caractéristiques \anglicisme{bottleneck} comme présentée dans la Figure~\ref{chap:tdae:fig:svbn}.

\begin{figure}[h!]
\begin{center}
\includegraphics[scale=0.60]{chapters/3-multidim/Graphics/svbn}
 \caption{Extraction de caractéristiques bottleneck dans un réseau supervisé.}
\label{chap:tdae:fig:svbn}
\end{center}
%\vspace{-3mm}
\end{figure}

Cette information de supervision permet d'obtenir des représentations propres à la tâche, ne modélisant que l'information utile à la classification. \cleaned{Ces représentations intermédiaires sont plus proches, ce qui met en évidence la présence de bruit. Celui-ci peut donc plus facilement être modélisé par un autoencodeur débruitant hétérogène.} % (espace bruité $\rightarrow$ espace propre).
La couche cachée de cet autoencodeur apprend une représentation intermédiaire débruitée plus facile à classifier et plus robuste au bruit. 

Dans le Chapitre~\ref{chap:sdae} nous avons présenté et utilisé des paramètres produits par une couche \anglicisme{bottleneck} d'un réseau de neurones artificiels entrainés dans un contexte \textbf{non supervisé}.
Les caractéristiques issues d'une couche \anglicisme{bottleneck} d'un réseau \textbf{supervisé} ont aussi été employées intensivement, en particulier pour le traitement de la parole. Elles ont été exploitées, par exemple, pour créer des descripteurs acoustiques robustes pour la transcription automatique~\cite{grezl2007}. On les retrouve aussi dans \cite{doddipatla2014speaker}, où des caractéristiques \anglicisme{bottleneck} ont été utilisées pour réaliser une adaptation locuteur du SRAP.
De la même manière, un empilement de couches \anglicisme{bottleneck} a permis de produire un ensemble de caractéristiques efficace pour la reconnaissance du langage~\cite{matejka2014neural}. D'autres travaux~\cite{ren2016combination} introduisent une comparaison des descripteurs obtenus par un perceptron multicouche (MLP) et par DAE ainsi que leurs différentes combinaisons. Ces analyses ont montré que, prises indépendamment, les caractéristiques provenant du MLP,dont l'information décorrélée de la tâche est supprimée, sont les plus efficaces des deux pour la déréverberation de la parole distante.

Dans un contexte multimodal, des descripteurs bimodaux ont été introduits~\cite{takashima2016audio}. Ils combinent des couches \anglicisme{bottlenecks} issues de deux réseaux supervisés pour fusionner l'information acoustique et l'information visuelle afin d'améliorer les performances de transcription automatique.
En synthèse vocale, des combinaisons de couches \anglicisme{bottleneck} construites avec des MLP ont amélioré les performances de systèmes automatiques~\cite{Wu2015DeepNN}.
D'autres travaux~\cite{parviainen2010dimension} ont montré que le MLP est un moyen efficace pour réduire la dimension des données d'entrée afin d'améliorer des modèles de régression.

%Dans ce but nous proposons une variation du SDAE dont la première étape de l'apprentissage utilise des caractéristiques bottleneck apprises par un MLP utilisant la supervision de classification. 

Pour introduire l'information de supervision dans le débruitage de documents transcrits automatiquement nous proposons une application originale des autoencodeurs, inspirée du SDAE présenté dans le Chapitre~\ref{chap:sdae}. Celle-ci, appelée autoencodeur débruitant avec des caractéristiques spécifiques à la tâche (Task specific denoising autoencoder, TDAE), exploite les capacités de modélisation de deux MLP : un premier pour créer un espace latent de documents propres, et un second pour créer espace latent de documents bruités. Ensuite, un troisième réseau de neurones est entrainé pour lier les deux espaces latents et  produire une représentation débruitée des documents ASR.

La Section \ref{chap:tdae:sec:tdae} introduit cette architecture et son processus d'apprentissage. La Section~\ref{chap:tdae:sec:exp} détaille les expériences réalisées pour évaluer les capacités de débruitage de cet autoencodeur pour la classification thématique de documents bruités. La Section~\ref{chap:tdae:sec:res} présente les résultats expérimentaux et leurs analyses.
Enfin, la Section~\ref{chap:tdae:sec:conclusion} conclut ce chapitre en faisant le bilan de ces travaux.% et propose des pistes d'ouvertures.

\section{Autoencodeur débruitant avec caractéristiques spécifiques à la tâche (TDAE)}
\label{chap:tdae:sec:tdae}
La problématique considérée est de construire une représentation robuste à partir de documents bruités ou corrompus produits par un SRAP. Nous proposons, dans ce chapitre, une architecture d'autoencodeur particulière (TDAE) pour apprendre cette représentation. D'abord, nous présenterons les spécificités  de cette architecture, ensuite nous aborderons les  particularités liées à son apprentissage.

\subsection{Spécificités du TDAE}
Cette architecture est capable de combiner lors de l'apprentissage deux formes de supervision. La première supervision est introduite lors de l'apprentissage de représentation latente homogène par un MLP. Cette information permet la sélection des informations conservées dans l'espace latent pour optimiser la classification. La seconde supervision est apportée par les documents transcrits manuellement (\anglicisme{TRS}) pour le débruitage des documents transcrits automatiquement (\anglicisme{ASR}).
Les expériences présentées dans le Chapitre~\ref{chap:sdae} ont montré que les caractéristiques latentes d'un même document appris dans des environnements différents sont plus proches et donc plus faciles à débruiter que les formes apparentes de surface. Le TDAE exploite des représentations latentes apprises par des MLP spécifiquement entrainés pour résoudre cette tâche de classification et propose un apprentissage en deux temps où chaque étape a un objectif différent. Contrairement au SDAE où trois autoencodeurs sont entrainés dans l'objectif de débruiter les représentations, nous proposons maintenant d'avoir une étape de sélection de la variabilité utile à la classification et ensuite un débruitage restreint à l'information pertinente. L'autoencodeur profond final capitalise ainsi sur les capacités de modélisation supervisées du MLP.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.70]{chapters/Graphs/TDAE}
 \caption{Architecture de l'autoencodeur débruitant avec des caractéristiques spécifiques à la tâche. À gauche, les deux réseaux préentrainés et à droite le réseau final.}
\label{chap:tdae:fig:tdae}
\end{center}
%\vspace{-3mm}
\end{figure}

L'architecture de ce réseau est présentée dans la Figure~\ref{chap:tdae:fig:tdae}. On peut voir à gauche les deux MLP simples entrainés indépendamment sur les données issues du SRAP et sur les données manuelles pour extraire l'information utile à la classification. Les poids des couches cachées $W^{(ASR)}$ et $W^{(TRS)}$ (en vert et jaune respectivement) sont transmises au TDAE complet présenté à droite du schéma. Le cadre en pointillé montre les deux matrices de poids $W^{(1)}$ et $W^{(2)}$ mises à jour lors de son apprentissage.
La méthode utilisée pour entrainer le TDAE est détaillée ci-dessous.

\subsection{Méthode d'entrainement du TDAE}
L'apprentissage du TDAE est réalisé en deux étapes. \\
D'abord, deux MLP homogènes sont entrainés sur la tâche finale de classification. Le premier prend comme vecteur d'entrée les données bruitées, le second quant à lui prend des données propres. Ils sont nommés $MLP_{ASR}$ et $MLP_{TRS}$ respectivement.
De même que pour le SDAE, les documents propres sont considérés comme la supervision du débruitage.
Ils vont déterminer une représentation réduite de leurs documents respectifs en conservant l'information utile pour résoudre la tâche dans chacun des environnements.
Ensuite, le TDAE, composé d'au moins trois couches cachées, appelées $h^{i}_{ASR}$, $h$ et $h^{i}_{TRS}$, est entrainé. \\
De la même manière que pour la construction d'un autoencodeur, pour augmenter la capacité de modélisation du réseau, l'optimisation du nombre de couches cachées avant et après la couche \anglicisme{bottleneck} $h$ peut être réalisée sur un corpus de développement. 
Les poids des matrices extérieures $W^{(ASR)}$ et $W^{(TRS)}$ du TDAE sont initialisés à partir des poids appris par $MLP_{ASR}$ et $MLP_{TRS}$. Ces matrices de poids seront fixées lors de l'apprentissage. Seules les matrices à l'intérieur de l'encadré pointillé ($W^{(1)}$ et $W^{(2)}$) sont affectées par la descente de gradient. Cette configuration permet de séparer les fonctions dans les différentes couches du réseau. Les couches extérieures sont utilisées pour construire des représentations latentes qui contiennent l'information utile du document. Les couches centrales ont pour rôle de débruiter ces représentations qui sont potentiellement plus similaires que les formes de surfaces initiales.
L'objectif de la couche \anglicisme{bottleneck} $h$ n'est pas de compresser l'information, mais d'apprendre une représentation robuste liant les deux espaces provenant d'environnements différents.

\section{Expérimentations}
\label{chap:tdae:sec:exp}

De manière à être comparable aux résultats des chapitres précédents, le TDAE est évalué sur la tâche TID présentée dans le Chapitre~\ref{chap:decoda}. Sur cette tâche plusieurs systèmes seront comparés : Le TDAE avec les deux MLP pour le préentrainement, une version du TDAE qui n'utilise pas les documents propres (MLP-AE), le SDAE et une analyse en composantes principales (Principale Component Analysis, PCA). En effet, dans le Chapitre~\ref{chap:lts} la méthode LTS qui utilise une analyse en composantes principales obtient, dans certains cas, de meilleures performances que l'autoencodeur pour la fusion et la sélection d'informations.

Les couches cachées des réseaux de neurones utilisent une fonction d'activation <<tanh>>. Leurs poids sont adaptés par descente de gradient <<Adam>>. Le nombre d'itérations pour l'apprentissage est déterminé par arrêt anticipé. Les autoencodeurs utilisent une fonction de cout $L_{MSE}$ telle que définie dans le Chapitre~\ref{chap:decoda}.

Les sections suivantes détaillent les paramètres et choix d'implémentations des différents réseaux entrainés. D'abord les MLP puis le TDAE et enfin le MLP-AE sont présentés.

\subsection{Perceptrons multicouches homogènes}

Les MLP homogènes ($MLP_{ASR}$, $MLP_{TRS}$) sont composés de trois couches cachées de taille 256, 128 et 256. Les tailles des différentes couches cachées sont déterminées en optimisant les résultats du TDAE sur le corpus de développement. Une régularisation à base de \anglicisme{Dropout} (c.f. Chapitre~\ref{chap:nnc}) est employée. Elle permet de limiter le surapprentissage des MLP et d'introduire un bruit additif comparable à celui des AE. % des chapitres précédents.
Ces deux MLP sont indépendants et peuvent être entrainés en parallèle.
Les couches cachées produites par $MLP_{ASR}$ sont utilisées comme \anglicisme{bottleneck} et évaluées aussi à des fins d'analyse. 


\subsection{TDAE}

Les couches $h^{i}_{ASR}$ et $h^{i}_{TRS}$ du TDAE sont initialisées avec les poids des MLP homogènes. Les poids de ces couches sont fixés à l'apprentissage. La couche $h$ centrale qui contient la projection finale est de taille $300$ comparable aux autres architectures. Ce réseau utilise en vecteur d'entrée les documents $x_{ASR}$ et en sortie les documents $x_{TRS}$.

Une seconde version du TDAE est évaluée aussi dans ces expériences. Cette variante propose de remplacer les fonctions d'activation des couches centrales du TDAE par une fonction relu. Les réseaux de neurones qui utilisent cette fonction d'activation tendent à avoir de meilleures performances (c.f. Chapite~\ref{chap:nnc}). La fonction tanh sélectionnée dans les expériences préliminaires a été choisie, car l'utilisation de la fonction relu résulte en une portion importante de neurones morts (dont l'état est à 0 systématiquement). Avoir des neurones morts dans un réseau implique une diminution drastique des capacités de modélisation de celui-ci.
Dans la nouvelle configuration avec un apprentissage en deux temps, le phénomène de neurones morts est réduit.
Nous proposons donc d'évaluer aussi un TDAE dont les couches cachées centrales utilisent cette fonction d'activation.
Pour différencier les deux réseaux, nous appellerons ces deux réseaux TDAE(tanh) et TDAE(relu).

\subsection{MLP-AE}

De manière à évaluer l'apport des transcriptions manuelles dans le processus de débruitage du TDAE, une version de ce même réseau est entrainée sans cette supervision. Le processus d'apprentissage est identique au TDAE à l'exception près que l'erreur de ce réseau est calculée sur les données bruitées au lieu des données propres. Cette configuration (MLP-AE) revient à mettre en cascade deux MLP et un autoencodeur en n'utilisant que des vues bruitées des documents.

\subsection{Analyse en composantes principales (PCA)}
Le nombre de composants principaux pour projeter les documents est sélectionné à postériori en fonction des performances en classification sur le corpus de développement. 

Les résultats de ces systèmes en classification sur le corpus de la tâche TID sont aussi comparés aux différents modèles présents dans le Chapitre~\ref{chap:sdae}. 

\section{Résultats expérimentaux}
\label{chap:tdae:sec:res}

\begin{table}[!ht]
\centering
\scalebox{1}{
\begin{tabular}{c|c|c|c||c}

{\bf Système} &  {\bf Vecteur} & {\bf Vecteur } & {\bf Couche } & {\bf Précision }\\
{\bf } & {\bf d'entrée} & {\bf de sortie} & {\bf utilisée} & {\bf (\%) }\\

\hline

SDAE & $x_{ASR}$/$W_{ASR}$ & $x_{TRS}$/$W_{TRS}$ & $h$ & { 83,2} \\
\hline
\hline
PCA & ASR & - & - & 82,5 \\

%  $AE_{ASR}$ & ASR & -- & $h$  & 81\\
%  $AE_{TRS}$ & TRS & -- & $h$  & 84 \\
%  \hline
%  \hline
% {\bf S}tacked & ASR & -- & $h^{(1)}$  & 81.7\\
% Deep & ASR & -- & $h^{(2)}$  & 82.0\\
% {\bf A}utoencoder & ASR & -- & $h^{(3)}$  & 80.1\\
% ({\bf SAE}) & ASR & -- & $h^{(4)}$  & 81.0\\
 \hline
 \hline
  & ASR & $Y'$ & $h^{1}$ & 70,4\\
 %\hline
 { $MLP_{ASR}$} & ASR & $Y'$ & $h^{2}$ & 71,3\\
 %\hline
  & ASR & $Y'$ & $h^{3}$ & 71,3\\
 %\hline


% \hline
% \hline

% {\bf F}inetuned & ASR & TRS & $h^{(\text{ASR})}$ & \textcolor{red}{69.7}\\
% %\hline
% {\bf S}upervised & ASR & TRS & $h$ & \textcolor{red}{76.5}\\
% %\hlinez
% {\bf A}utoencoder  & ASR & TRS & $h^{(\text{TRS})}$ & \textcolor{red}{73.4}\\
% %\hline
% ({\bf FSDAE}) & ASR & TRS & $\tilde{x}$ & \textcolor{red}{71.9}
% \\


\hline
\hline
 & ASR & ASR & $h^{1}$ & {80.4}\\
%\hline
{\bf MLP-AE}& ASR & ASR & $h^{2}$ & { 81.3}\\
%\hlinez
 & ASR & ASR & $h^{3}$ & {80.4}\\
%\hline



\hline
\hline
 & $x_{ASR}$/$W_{ASR}$ & $x_{TRS}$/$W_{TRS}$ & $h^{i}_{ASR}$ & {82,3}\\
%\hline
  TDAE (tanh) & $x_{ASR}$/$W_{ASR}$ & $x_{TRS}$/$W_{TRS}$ & $h$ &  84,1 \\
%\hline
 & $x_{ASR}$/$W_{ASR}$ & $x_{TRS}$/$W_{TRS}$ & $h^{i}_{TRS}$ & {83,2}\\
%\hline

\hline
\hline
 & $x_{ASR}$/$W_{ASR}$ & $x_{TRS}$/$W_{TRS}$ & $h^{i}_{ASR}$ & {83,2}\\
%\hline
{\color{SkyBlue} TDAE (relu) } & $x_{ASR}$/$W_{ASR}$ & $x_{TRS}$/$W_{TRS}$ & $h$ & { \color{SkyBlue} \bf 85,2}\\
%\hlinez
 & $x_{ASR}$/$W_{ASR}$ & $x_{TRS}$/$W_{TRS}$ & $h^{i}_{TRS}$ & {83,4}\\
%\hline


\hline
\hline

\end{tabular}
}
\caption{Précisions des différents systèmes présentés sur la tâche de classification thématique de documents parlés TID. Dans ce tableau $Y'$ correspond à l'hypothèse de classification.}
\label{chap:tdae:tab:result}

\end{table}

Les résultats de ces expérimentations sont présentés dans le Tableau~\ref{chap:tdae:tab:result}. On peut remarquer dans ce tableau que les meilleures performances sont obtenues par les deux configurations du TDAE. La variation avec relu surpasse la version originale avec $85,2$\% et $84,1$\% respectivement.

Avec les deux variations du TDAE, les meilleures performances sont obtenues en utilisant les projections produites dans la couche centrale ($h^{2}$) des $MLP_{ASR}$ et $MLP_{TRS}$. 

Les résultats du SDAE sont les troisième meilleurs avec $83,2$\%, soit une différence relative de presque $1,1$\% par rapport au TDAE qui utilise des paramètres comparables (tanh).

Le MLP-AE obtient une précision de $81,3$\% ce qui correspond à une perte absolue de $2,8$\% comparativement au TDAE. Les documents transcrits manuellement sont donc utiles aux processus de débruitage.

Les systèmes utilisant les couches intermédiaires de $MLP_{ASR}$ comme caractéristiques obtient le plus mauvais score de ce jeu d'expérience avec aux mieux $71,3$\% de réussite. C'est une perte de $6,7$ points par rapport à un système qui réalise la classification  directement avec $MLP_{ASR}$ et obtient le score de 78\%.

Enfin, les descripteurs projetés dans les composantes principales d'une PCA obtiennent le score de $82,5$\%. Ce score est obtenu en utilisant les 26 premières composantes principales. \cleaned{Il s'agit du meilleur résultat obtenu sans supervision parmi les systèmes présentés dans le tableau.}

La Figure~\ref{chap:tdae:fig:time_tdae} compare le temps d'apprentissage nécessaire pour le TDAE avec ceux du SDAE. On peut voir dans ce schéma que le TDAE peut être entrainé de deux manières, soit en parallèle, ce qui signifie que les deux MLP ($MLP_{ASR}$ et $MLP_{TRS}$) sont entrainés en simultané, soit en séquentiel en entrainant les réseaux les uns après les autres. Dans les deux conditions, les temps d'apprentissage sont relativement similaires. Les gains apportés par le TDAE ne s'obtiennent pas au prix d'une augmentation du temps d'apprentissage, mais consomment plus de ressources mémoires.

\begin{figure}[h!]
\includegraphics[scale=0.6]{chapters/3-multidim/Graphics/time_tdae}
 \caption{Temps de calcul nécessaire aux apprentissages du TDAE et du SDAE.}
\label{chap:tdae:fig:time_tdae}
%\vspace{-3mm}
\end{figure}


% \begin{table}[!ht]
% \centering
% \scalebox{1}{
% \begin{tabular}{c|c|c|c||c}

% {\bf Autoencoder} &  {\bf Input} & {\bf Output} & {\bf Layer} & {\bf Test}\\
% {\bf employed} & {\bf } & {\bf } & {\bf vector} & {\bf Accuracy}\\
% \hline
%  $AE_{ASR}$ & $x_{ASR}$ & $x_{ASR}$ & $h$  & 81\\
%  $AE_{TRS}$ & $x_{TRS}$ & -- & $h$  & 84 \\
%  \hline
%   SAE & $x_{ASR}$ & $x_{ASR}$ & $h^{(2)}$  & 82.0\\
% \hline
% DDAE & $x_{ASR}$ & $x_{TRS}$ & $h^{(1)}$ & 72.5\\

% \hline
% %\hline
% {\bf S}FSDAE & $x_{ASR}$ & $x_{TRS}$ & $h$ & 76.5\\
% \hline
% \hline
% {\bf T}ask-specific & ASR & TRS & $h^{1}$ & {82.3}\\ 
% %\hline
% {\bf D}enoising & ASR & TRS & $h^{2}$ & {\bf 84.1}\\  {85.2}
% %\hlinez
% {\bf A}utoencoder  & ASR & TRS & $h^{3}$ & {83.2}\\  {83.4}
% %\hline
% ({\bf TDAE}) &  &  &  & \\
% \hline
% \hline
% Proposed {\bf SDAE} & $x_{ASR}$/$W_{ASR}$ & $x_{TRS}$/$W_{TRS}$ & $h$ & \emph{ 83.2}\\
% \end{tabular}
% }


%\label{chap:tdae:tab:result}
%\end{table}

\section{Conclusion}
\label{chap:tdae:sec:conclusion}


Dans ce chapitre nous avons proposé d'introduire de la supervision via des références de classification combinée à la supervision de débruitage par l'intermédiaire des documents transcripts manuellement lors de la construction de représentations robustes. Ces représentations sont exploitées ensuite pour la classification thématique de documents parlés. Elles sont construites par un autoencodeur profond dont les poids des couches extérieures sont préentrainés et figés lors de l'apprentissage final.
Les préentrainements sont réalisés à l'aide des couches \anglicisme{bottleneck} de deux MLP, le premier est entrainé sur les documents propres issus de transcriptions manuelles et le second sur les documents bruités produit par le SRAP. Ces réseaux de neurones ont pour objectif de sélectionner l'information pertinente pour la classification.
Ensuite le TDAE complet apprend à reconstruire une version propre des documents à partir de leurs versions bruitées. La couche cachée du TDAE apprend une représentation intermédiaire qui lie l'espace bruité à l'espace propre. 

Les représentations construites par le TDAE sont plus efficaces pour la classification de documents parlés. Un gain relatif atteignant $10,5$\% est mesurable en utilisant ces représentations par rapport à celles des documents initiaux. L'utilisation de représentations intermédiaires, lorsqu'elles sont construites avec la supervision de la tâche par les couches \anglicisme{bottleneck} d'un MLP, rend le débruitage plus simple et permet une meilleure classification qu'un apprentissage non supervisé d'autoencodeur.
Avec ces descripteurs, l'exploitation des documents bruités ne provoque plus qu'une perte relative de $0,11$\% comparés à des représentations abstraites propres nettoyées.  Les erreurs induites par le SRAP sont en majorité gommées par notre système d'extraction de caractéristiques débruitées.

Nous voyons plusieurs perspectives intéressantes à ces travaux, dont deux sont communes au SDAE.

La première ouverture, commune avec le SDAE, que nous proposons concerne la représentation des données d'origine.
Cette méthode ne pose pas de conditions à priori sur le type de représentations utilisé. De plus, nous avons montré dans le Chapitre~\ref{chap:lts} que l'utilisation d'autoencodeurs sur des représentations issues de modèles \anglicisme{Author-Topic} a un intérêt. L'utilisation de ces méthodes de débruitage sur des représentations de plus haut niveau pourrait avoir un effet positif sur les résultats globaux de la tâche.

La seconde ouverture repose sur des représentations qui modélisent la structure des documents construites par des LSTM~\cite{graves2012supervised} ou des GRU~\cite{chung2014empirical}. Appliquées aux sorties de SRAP, elles pourraient modéliser aussi les erreurs de transcriptions et de reporter les erreurs du SRAP sur la tâche de compréhension du langage. L'intégration du débruitage lors de l'entrainement de ces structures permettrait de détecter le bruit, de prévenir ses effets plus facilement, et par conséquent de construire des représentations plus robustes et plus informatives.

La troisième ouverture, commune avec le SDAE, serait de travailler avec un corpus de données plus volumineux. Un corpus sensiblement plus important justifierait l'emploi de méthodes plus profondes. De plus l'utilisation des couches \anglicisme{bottleneck} est moins efficace lorsque beaucoup de données d'apprentissage sont disponibles. Valider (ou non) le comportement mis en évidence sur la tâche TID serait utile. Mais un corpus annoté thématiquement et transcrit manuellement suffisamment volumineux est difficile à trouver ou collecter.

Nous avons utilisé deux types de préentrainement dans nos travaux. L'application des méthodes d'apprentissage joint~\cite{caruana1998multitask} \cite{collobert2008unified} est une piste intéressante qui permettrait d'améliorer les systèmes de débruitage que nous avons présenté et d'en simplifier le processus, puisqu'il n'y aurait plus qu'un seul apprentissage, tout en conservant ses propriétés. Cette méthode est à étudier en particulier dans les cas où les données d'apprentissage sont suffisamment conséquentes pour réduire le gain observé des couches \anglicisme{bottleneck} comparées à un réseau plus profond.

Enfin les architectures récentes GAN et VAE proposent des outils puissants pour apprendre des modèles génératifs. Ces modèles pourraient être une piste d'étude intéressante pour fournir des alternatives aux autoencodeurs débruitants. Ces méthodes pourraient reconstruire des documents lisibles, au lieu de représentations latentes, ce qui peut faciliter l'interprétation des choix du système automatique et ouvrir de nouvelles applications.

Nous avons présenté une nouvelle architecture qui utilise deux visions des documents pour apprendre à créer des représentations aussi efficaces que celles construites à partir de documents propres. Nous avons aussi proposé plusieurs pistes d'évolution à ces travaux. 
Le chapitre suivant va conclure sur l'ensemble des travaux présentés dans ce manuscrit, des perspectives plus globales seront ensuite introduites et discutées.