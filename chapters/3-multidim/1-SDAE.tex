\chapter{Supervision de l'apprentissage des autoencodeurs pour améliorer la compréhension de la parole. }
\label{chap:sdae}
\minitoc
\section{Introduction}
\label{chap:sdae:sec:introduction}

Les chapitres précédents ont présenté et adapté des méthodes de classification thématique de documents parlés.
Une des principales difficultés rencontrées par un système qui repose sur des transcriptions automatiques est de gérer les taux d'erreurs importants introduits par les SRAP en amont.
Le Chapitre~\ref{chap:sae} a montré que des DAE peuvent être utilisés pour créer des représentations latentes robustes des documents transcrits automatiquement.
Ces représentations sont construites de manière à être plus réduites, plus denses, moins sensibles aux \cleaned{variabilités. Elles mettent en évidence les différences interdocuments. % que les représentations en sacs de mots initiales du chapitre \ref{chap:decoda}.
et permettent ainsi d'améliorer les performances globales du système de SLU.}
Toujours dans le Chapitre~\ref{chap:sae}, nous avons introduit des autoencodeurs débruitants profonds (DDAE) qui apprennent à reconstruire les transcriptions manuelles à l'aide de transcriptions automatiques. Cependant, les résultats obtenus avec les représentations latentes construites par ces DDAE se sont révélés décevants. L'hypothèse qu'un système, qui connait les représentations idéales (oracle) et corrompues d'un même élément, puisse en déduire les patrons que suit le processus de corruption de l'information parait pourtant réaliste. Nous en avons déduit que les erreurs introduites par le SRAP sont trop nombreuses et trop codépendantes pour qu'un DDAE puisse les modéliser aussi directement.

%Pour surmonter cette difficulté,  %L'apprentissage de ce réseau de neurones artificiels se déroule en deux phases pour apporter une robustesse supérieure aux réseaux de neurones profonds définie dans le Chapitre~\ref{chap:sae} et pour conserver une capacité d'abstraction importante.

\begin{figure}[h!]
\begin{center}
\includegraphics[scale=0.70]{chapters/3-multidim/Graphics/apprentissage2temps}
 \caption{Processus d'utilisation de caractéristiques Bottleneck.}
\label{chap:sdae:fig:2temps}
\end{center}
%\vspace{-3mm}
\end{figure}
Des méthodes d'apprentissage de réseaux de neurones reposent sur un apprentissage en deux processus distincts. Un premier réseau est entrainé pour l'extraction de caractéristiques propres aux données d'entrainement. Alors qu'un second réseau utilise la projection produite par le premier pour réaliser la tâche traitée. Les vecteurs produits lors de la projection sont appelés caractéristiques \anglicisme{bottleneck}. Le terme anglais \anglicisme{bottleneck} se traduit par goulot d'étranglement ou entonnoir en français.  
La Figure~\ref{chap:sdae:fig:2temps} présente cette idée. Dans ce schéma, l'étape $1$ correspond à l'apprentissage d'un réseau pour la génération de représentation latente. La fonction objectif utilisée par celui-ci est variable, elle peut être identique~\cite{grezl2007} ou différente~\cite{Tan2015BottleneckFF} de l'objectif de la tâche finale. Durant l'étape $2$ , les documents d'entrainements sont projetés dans la couche bottleneck (représentée par l'encadré bleu). Lors de l'étape $3$, le second réseau est entrainé pour résoudre la tâche en utilisant comme vecteurs d'entrée les projections construites durant l'étape $2$. L'étape $4$ montre les étapes successives nécessaires pour réaliser une prédiction pour un nouveau document en utilisant des caractéristiques bottleneck.

La Figure~\ref{chap:sdae:fig:bottle} présente la génération des caractéristiques \anglicisme{bottleneck} \cleaned{correspondant} à l'étape $2$ du précédent schéma. Le plus souvent, ce terme fait référence à la plus petite couche cachée positionnée au centre du réseau. Ce n'est cependant pas une obligation~\cite{doddipatla2014speaker}, \cite{Carcenac2015AHS}, \cite{zhang2014language}. Chaque couche cachée d'un réseau de neurones profonds génère des représentations avec un niveau de compression. La couche cachée optimale à utiliser peut être sélectionnée en fonction d'une interprétation de sa capacité de représentation~\cite{grezl2007} ou en fonction de ses résultats sur un corpus de développement.

La formulation des autoencodeurs dans le Chapitre~\ref{chap:sae} peut être vue comme un cas particulier de cette méthode dont le premier réseau de neurones artificiels est composé d'un encodeur et d'un décodeur. C'est le cas aussi des \anglicisme{Word2vec} présentés dans le Chapitre~\ref{chap:w2d}.

\begin{figure}[h!]
\begin{center}
\includegraphics[scale=0.50]{chapters/3-multidim/Graphics/Bottleneck}
 \caption{Génération de caractéristiques Bottleneck.}
\label{chap:sdae:fig:bottle}
\end{center}
%\vspace{-3mm}
\end{figure}

Cette méthode permet d'éviter la disparition du gradient. Durant l'étape de rétropropagation, le gradient devient de plus en plus faible au fur et à mesure qu'il est propagé dans les couches les plus profondes. Si un réseau profond est séparé en deux parties moins profondes, avec chacune leur propre descente de gradient, le phénomène de disparition est alors moins important.
Lorsque le premier réseau est entrainé avec un objectif propre, par exemple avec l'emploi d'un autoencodeur, elle apporte des avantages supplémentaires. D'abord, elle permet de s'assurer que le second réseau conserve de bonnes capacités de généralisation. Enfin, elle réduit le risque de surapprentissage et converge vers un minimum local éloigné de l'optimum recherché.
Ce second effet est notamment utile quand la quantité de données disponibles est un facteur limitant pour l'apprentissage d'un réseau de neurones profonds~\cite{zhang2014language}.\\
En contrepartie, le nombre de métaparamètres à déterminer et le temps nécessaire à l'apprentissage augmentent considérablement.

L'utilisation de paramètres issus d'une couche \anglicisme{bottleneck} obtient de bons résultats dans des domaines de recherche variés.
Par exemple, dans le traitement de la parole, \cite{Tan2015BottleneckFF} utilisent des enchainements de \anglicisme{bottleneck} produits par des réseaux débruitants. Cet enchainement de réseaux construit des modèles de locuteurs robustes pour une reconnaissance efficace appliquée à une tâche où les données disponibles ne permettent pas d'entrainer des réseaux très profonds. \cite{Tian2015StackedBF} exploitent le même principe pour proposer une alternative aux i-vecteurs et à l'analyse linéaire discriminante probabiliste~(PLDA). En reconnaissance de la parole, cette méthode a permis de diminuer considérablement les ``taux d'erreurs mots''. Elle est employée aussi pour mélanger des modèles génératifs et discriminants dans un même modèle~\cite{Hinton2012DeepNN}. Cette combinaison permet d'obtenir des modèles de SLU avec de meilleures capacités de généralisation.

Dans la littérature, cette forme d'apprentissage est aussi employée pour lier des domaines de représentations très différents alors que les relations entre les espaces ne sont pas évidentes. \cite{Chuangsuwanich2016MultilingualDS} et \cite{Fr2015MultilingualBF} proposent des réseaux de neurones qui génèrent des caractéristiques multilingues exploitées par des systèmes de reconnaissance de la parole.
\cite{Wu2015DeepNN} proposent un outil de synthèse de la parole qui s'appuie sur des \anglicisme{bottleneck} pour créer un espace de représentation unique qui contient des informations à la fois sur les graphèmes d'entrée et les hypothèses de phonèmes à produire.
Elle est exploitée aussi sur des tâches de traitement de l'image, notamment dans \cite{Carcenac2015AHS}, où les auteurs proposent de découper un réseau de neurones en plusieurs ``modules''. Chaque module traite un niveau de résolution différent. Les modules de bas niveau sont combinés comme entrée pour les modules de haut niveau afin de former une chaine de traitement capable d'appliquer des transformations complexes sur l'image d'origine.

Dans ce chapitre, un autoencodeur original utilisant un apprentissage en deux temps, de manière à exploiter conjointement les transcriptions manuelles et les transcriptions automatiques, est proposé. Dans un premier temps, deux DAE sont entrainés pour débruiter les représentations propres et corrompues des données. Ensuite, un troisième réseau est entrainé pour créer un espace intermédiaire liant les deux représentations préalablement apprises.
La section \ref{chap:sdae:section:sdae} introduit cet autoencodeur et les particularités de son apprentissage. La Section~\ref{chap:sdae:sec:exp} présente les expériences réalisées pour évaluer les capacités de débruitage de cet autoencodeur pour la compréhension de la parole. La Section~\ref{chap:sdae:sec:res} détaille les résultats de ces expériences qui sont analysées et commentées Section~\ref{chap:sdae:subsec:discusion}.

\section{Autoencodeur profond supervisé (SDAE)}
\label{chap:sdae:section:sdae}
\subsection{Intérêt du SDAE}
La problématique considérée est toujours de construire une représentation robuste à partir de documents bruités ou corrompus issus du SRAP. Nous proposons un autoencodeur profond supervisé (Supervised Deep Autoencoder, SDAE) pour extraire cette représentation.
Cette variation originale des autoencodeurs est capable d'utiliser la supervision offerte par les documents transcrits manuellement (\anglicisme{TRS}) pour le débruitage des documents transcrits automatiquement (\anglicisme{ASR}). Les expériences du Chapitre~\ref{chap:sae} ont montré qu'une approche classique qui emploie des AE profonds pour apprendre à reconstruire les documents propres échoue. En effet, ils sont incapables de produire des représentations améliorant la classification. Cette faiblesse s'explique par l'écart entre les deux représentations, trop important pour créer une représentation intermédiaire utile. Les AE débruitants homogènes (entrée et sortie de la même source) sont capables de créer des représentations latentes denses et efficaces. De la même façon que le SAE empile sur ces AE débruitants, il est important que le réseau proposé capitalise cette force. Le SDAE utilise un apprentissage en plusieurs temps pour créer un lien entre les deux espaces propres et bruités.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.70]{chapters/Graphs/SDAE}
 \caption{Architecture de l'autoencodeur profond supervisé. À gauche, les deux réseaux préentrainés et à droite le réseau final.}
\label{chap:sdae:fig:SDAE}
\end{center}
%\vspace{-3mm}
\end{figure}

L'architecture de ce réseau est présentée dans la Figure~\ref{chap:sdae:fig:SDAE}. On peut voir à gauche les deux autoencodeurs simples entrainés indépendamment sur les données issues du SRAP et sur les données manuelles. Les poids des couches cachées $W^{(ASR)}$ et $W^{(TRS)}$ (en vert et jaune respectivement) sont transmises au SDAE complet présenté à droite du schéma. Le cadre en pointillé montre les seules matrices de poids utilisées pour son apprentissage.
La méthodologie utilisée pour entrainer le SDAE est détaillée ci-dessous.

\subsection{Particularités de l'entrainement du SDAE}
L'apprentissage du SDAE se déroule en deux phases.

D'abord, deux DAE homogènes classiques sont entrainés. Le premier prend en vecteur d'entrée et de sortie les données bruitées. Le second quant à lui prend des données propres. Ces dernières sont considérées comme la supervision naturelle du SDAE. Ils sont nommés $AE_{corrompu}$ et $AE_{propre}$ respectivement. Ils vont déterminer une représentation robuste de leurs documents respectifs qui  conserve un maximum de variabilités utiles et ignore l'information non pertinente.

Ensuite, le SDAE, composé de trois couches cachées appelées $h_{corrompu}$, $h$ et $h_{propre}$, est entrainé.
Les poids de ses matrices extérieures $W_{corrompu}$ et $W_{propre}$ du SDAE sont initialisés à partir des poids appris par $AE_{corrompu}$ et $AE_{propre}$ et sont fixés lors de l'apprentissage. Seules les matrices à l'intérieur de l'encadré pointillé ($W^{(1)}$ et $W^{(2)}$) sont affectées par la descente de gradient. Ainsi les capacités de débruitage apprises par $AE_{corrompu}$ et $AE_{propre}$ ne sont pas détériorées.
La couche \anglicisme{bottleneck} $h$ qui lie l'espace bruité à l'espace propre a besoin d'être particulièrement large. En effet, la mise en place d'une telle architecture n'a de réel intérêt que dans le cas où le lien entre les deux espaces peut être qualifié de complexe. Dans ce contexte, un autoencodeur ne risque pas d'approximer la fonction identité~\cite{baldi2012autoencoders} et la compression de l'information ne fait pas partie des objectifs. Il n'est donc pas gênant d'avoir une couche intermédiaire plus grande que celle d'entrée et de sortie. La taille idéale de cette couche peut être déterminée, comme d'autres métaparamètres, à l'aide d'un corpus de développement.




\section{Expériences}
\label{chap:sdae:sec:exp}
\subsection{Protocole expérimental}

Cette expérience est réalisée sur la tâche TID présentée dans le Chapitre~\ref{chap:decoda}. Les représentations TF.IDF.GINI présentées dans ce même chapitre sont compressées et débruitées par un autoencodeur. Ensuite un MLP classifie les documents débruités parmi 8 catégories.

Dans cette expérience, plusieurs autoencodeurs sont comparés sur la tâche TID. Ils utilisent tous une stratégie d'apprentissage différente pour mettre en évidence chacune des particularités du SDAE. Pour faciliter la compréhension, les deux autoencodeurs simples du SDAE sont entrainés séparément et ajoutés à la comparaison. Ils servent de préentrainement à plusieurs des AE profonds.

L'ensemble des réseaux utilisent les mêmes paramètres que dans le Chapitre~\ref{chap:sae}. C'est-à-dire qu'ils utilisent :  des fonctions d'activation de type <<tanh>>, un bruit de masquage qui affecte aléatoirement 50\% des neurones de la couche d'entrée pour produire de meilleures représentations intermédiaires, mais aussi pour réduire le surapprentissage. Les poids sont adaptés par descente de gradient <<Adam>>. Le nombre d'itérations est déterminé par arrêt anticipé lorsque la fonction de cout ($L_{MSE}$) ne réduit plus.

\subsection{les DAE simples}
L'$AE_{corrompu}$ utilise les documents issus du SRAP $x_{ASR}$. Il est appelé $AE_{ASR}$ dans ce contexte. l'$AE_{TRS}$ est entrainé pour débruiter les documents transcrits manuellement $x_{TRS}$. Il est donc nommé $AE_{TRS}$. Ces deux AE ont la même architecture que celle définie dans le Chapitre \ref{chap:sae}. \\
%Pour rappel, ils ont une couche cachée $h$ de taille $50$ et une fonction d'activation ``tanh'' choisie expérimentalement sur le corpus de développement. Un bruit par masquage est employé pour augmenter la robustesse des représentations intermédiaires et réduire le surapprentissage. Ils utilisent une fonction de cout $L_{MSE}$, une descente de gradient ``ADAM'' et un arrêt prématuré.\\
Ces deux AE consomment peu de ressources de calcul et sont indépendants, ils peuvent aisément être entrainés en parallèle.

\subsection{Le SDAE}

Le SDAE est composé de trois couches cachées. Les couches $h_{ASR}$ et $h_{TRS}$ sont initialisées avec les poids d'encodage des réseaux simples ($AE_{ASR}$, $AE_{TRS}$). Elles sont donc de taille $50$ aussi. La couche $h$ centrale qui contient la projection finale est de taille $300$, comparable au SAE présenté dans le Chapitre~\ref{chap:sae}. Ce réseau utilise en vecteur d'entrée les documents $x_{ASR}$ et en sortie les documents $x_{TRS}$.


\subsection{Le FSDAE}

L'autoencodeur supervisé profond réadapté (Finetuned Supervised Deep Autoencodeur, FSDAE) est un réseau de neurones profonds dont l'architecture est identique au SDAE. L'unique particularité du FSDAE est que lors de l'apprentissage final, les matrices de poids $W_{ASR}$ et $W_{TRS}$ sont adaptées pour la reconstruction des documents TRS. Nous faisons l'hypothèse ici que l'adaptation des poids dégrade les performances du système. En effet, ce mécanisme peut permettre au classifieur de trouver une meilleure solution grâce à l'information apprise par les AE simples. Mais il prend le risque que la répercussion des erreurs de reconstruction des documents propres soit toujours trop complexe pour le réseau, et le fasse retomber dans une situation similaire au DDAE en impactant la matrice $W_{ASR}$ qui permet déjà d'extraire une représentation robuste des transcriptions automatiques.


\section{Résultats}
\label{chap:sdae:sec:res}
\begin{figure}[h!]
\begin{center}
\includegraphics[scale=0.8]{chapters/3-multidim/Graphics/Hist_SDAE}
 \caption{Performances par couche cachée et par réseau sur la tâche TID. Les caractéristiques $AE_{TRS}$ (en jaune) sont les seules apprises et évaluées uniquement avec les documents transcrits manuellement.}
\label{chap:sdae:fig:histSDAE}
\end{center}
%\vspace{-3mm}
\end{figure}

La Figure~\ref{chap:sdae:fig:histSDAE} montre les précisions en classification obtenues par le SDAE proposé, FSDAE et les autoencodeurs profonds présentés dans le Chapitre~\ref{chap:sae}. La qualité des réseaux est évaluée sur la tâche TID en évaluant toutes les représentations intermédiaires produites par chacun des autoencodeurs. Les poids des couches cachées $h^{(\text{ASR})}$ et $h^{(\text{TRS})}$ du SDAE sont préentrainés puis fixés lors de l'apprentissage. Elles sont donc identiques aux couches intermédiaires de $AE_{ASR}$ et $AE_{TRS}$ respectivement, seule la couche $h$ du SDAE est informative.

Le SDAE obtient une précision de $83,2\%$. C'est le meilleur système utilisant les documents ASR. Ces résultats sont inférieurs de $1\%$ absolu comparativement aux résultats obtenus par $AE_{TRS}$ ce qui réduit encore l'écart de qualité d'information contenue entre les représentations bruitées et les représentations propres. Les performances se rapprochent fortement de celles obtenues en utilisant les documents TRS ($84,1\%$ et $85,3\%$) qui représentent un cadre sans erreur de transcription. Ils représentent donc l'objectif à réaliser pour les systèmes qui exploitent les transcriptions automatiques.

Comme attendu, les résultats du FSDAE montrent que ne pas fixer les matrices $W^{(\text{ASR})}$ et $W^{(\text{TRS})}$ lors de l'apprentissage final dégrade la capacité de modélisation du réseau de neurones. En effet, le FSDAE ne réalise qu'un score de $76,5\%$ en utilisant la projection produite par la couche cachée centrale $h_{2}$. Les couches cachées $h_1$ et $h_3$ qui utilisent le préentrainement obtiennent malgré cela une précision de $69,7\%$ et $73,4\%$. Les représentations de ces deux couches sont largement dégradées lors du réapprentissage.

Pour rappel, les performances du DDAE qui n'utilisent pas de méthode de préentrainement oscillent entre $69,4$ et $72,5\%$.
Ces deux derniers systèmes sont en dessous des performances de départ ($77,1\%$).

Le SAE avec ses $82,0\%$ de précision est le deuxième meilleur système, seulement $1,2$ point inférieur au SDAE.

\begin{figure}[htb]
\begin{center}
\includegraphics[scale=0.60]{chapters/3-multidim/Graphics/CONV}
 \caption{Proportions $ \frac{Performance_i}{Performance_{Max}}$ en fonction des itérations pour les MLP utilisant les caractéristiques des Autoencodeurs sans \anglicisme{early stopping}}.
\label{chap:sdae:fig:convSDAE}
\end{center}
%\vspace{-3mm}
\end{figure}

La Figure~\ref{chap:sdae:fig:convSDAE} montre le nombre d'itérations nécessaires pour obtenir les résultats optimaux avec les caractéristiques de base (TF.IDF) et celles générées par certains autoencodeurs. Les trois caractéristiques réagissent de façon similaire. La vitesse de convergence est forte jusqu'à environ 50 itérations. Ensuite les MLP apprennent peu et les performances stagnent ou se dégradent.


\begin{figure}[htb]
\includegraphics[scale=0.6]{chapters/3-multidim/Graphics/time}
 \caption{Temps de calcul nécessaire aux apprentissages des différents Autoencodeurs profonds.}
\label{chap:sdae:fig:timeSDAE}
%\vspace{-3mm}
\end{figure}

L'histogramme présenté Figure~\ref{chap:sdae:fig:timeSDAE} représente les temps nécessaires pour réaliser l'apprentissage de chacun des réseaux de neurones sur le matériel présenté Chapitre~\ref{chap:sae}. Le SAE a toujours le temps de calcul le plus long, 50 minutes. En effet, il nécessite trois apprentissages intermédiaires puis un apprentissage global. Ces apprentissages doivent être réalisés séquentiellement puisque chaque couche cachée dépend de la précédente. Le SDAE est second : il apparait deux fois dans le tableau, une fois en entrainant les réseaux intermédiaires en séquentiel (35 minutes) et une fois en parallèle. En entrainant les réseaux en parallèle, le SDAE converge en 25 minutes, soit deux fois moins que le SAE et l'équivalent d'un réseau débruitant standard (DDAE).
Les autoencodeurs simples sont évidemment les plus rapides avec un seul entrainement de 10 minutes.
Similairement au Chapitre~\ref{chap:sae} quand les temps d'apprentissages sont un critère important, les AE simples sont envisageables, car leur apprentissage est rapide pour des performances acceptables.
\section{Discussion}
\label{chap:sdae:subsec:discusion}

%ALL RESULTS ...
\begin{table}[!ht]
\centering
\scalebox{1}{
\begin{tabular}{c|c||c}
 \multicolumn{3}{c}{Données d'entrée ASR} \\
 \hline
{\bf Méthode} & {\bf Vecteur } & {\bf Précision}\\
{\bf employée} & {\bf d'entrée} & {\bf obtenue}\\
\hline
DDAE & $h^{(1)}$ & 72,5\\
DAE & $h$ & 74,3\\
%\hline
FSDAE  & $h$  & 76,5\\
%\hline
TF.IDF & -- & 77,1 \\
%\hline
$\text{AE}_{\text{ASR}}$ & $h$ & 81\\
SAE & $h^{(2)}$  & 82,0\\
\hline
\hline
{\bf \color{SkyBlue} SDAE} & $h$ &  { \color{SkyBlue} \bf 83,2}\\
\hline
%Proposed {\bf TDAE} & $h^2$ & {\bf 84,1}\\

\end{tabular}
}
\begin{tabular}{c|c||c}
 \multicolumn{3}{c}{Données d'entrée TRS} \\
 \hline
{\bf Méthode} & {\bf Vecteur} & {\bf Précision}\\
{\bf employée} & {\bf d'entrée} & {\bf obtenue}\\
\hline
AE (TRS) & $h$ & 84,1\\
SAE (TRS) & $h^{(3)}$ & 85,3\\
\hline
\end{tabular}

\caption{\label{chap:sdae:table:Accuracy_Recap_Decoda} Récapitulatif des meilleures performances (\%) observées pour chaque réseau de neurones artificiels sur la tâche TID.}

\end{table}

Le Tableau~\ref{chap:sdae:table:Accuracy_Recap_Decoda} compare les meilleures précisions réalisées sur la tâche TID pour chacune des architectures proposées.
Il est intéressant de souligner que le $\text{AE}_{\text{ASR}}$ et le SAE ont des performances globalement bonnes. C'est d'autant plus le cas que le premier est particulièrement simple et tous deux sont entrainés de façon entièrement non supervisée. Ces résultats sont obtenus grâce aux capacités de filtrage et de sélection de l'information des autoencodeurs débruitants qui ont permis de nettoyer une bonne partie du bruit présent dans les documents transcrits automatiquement.
Néanmoins, aucune de ces méthodes n'est capable d'obtenir des résultats semblables à l'utilisation des transcriptions manuelles.
Les résultats des SAE introduits dans le Chapitre~\ref{chap:sae} apportent un gain relatif de $2,7\%$ sur les documents TRS et de $6,4\%$ sur les documents transcrits automatiquement.
Ces différences indiquent qu'une partie du bruit introduit par la transcription automatique est capturée, et avec celui-ci un bruit intrinsèque aux documents qui est également présent dans les transcriptions manuelles est retiré. %C'est le bruit restant que le SDAE doit capturer.

La précision obtenue par le SDAE atteint $83,2\%$, seulement $0,9\%$ en dessous de la représentation propre utilisée comme objectif ($AE_{TRS}$ avec $84,1\%$). L'introduction de la supervision dans le processus de débruitage permet au réseau de modéliser et capturer une forme de bruit non traitée par les méthodes non supervisées et améliore ainsi la qualité de la représentation produite. Cette amélioration permet au système réel d'être compétitif avec les résultats obtenus à partir de la transcription oracle.

Enfin, les résultats faibles des DDAE et FSDAE sont eux aussi reportés dans le Tableau~\ref{chap:sdae:table:Accuracy_Recap_Decoda}. Ils montrent que les bruits cumulés présents dans les documents transcrits automatiquement sont difficiles à supprimer même pour un réseau profond qui traite l'information sur différents niveaux d'abstraction.

La force du SDAE vient des couches extérieures qui capitalisent sur les capacités de débruitage de ${AE}_{\text{ASR}}$ et $\text{AE}_{\text{TRS}}$. En effet, ces deux réseaux produisent deux représentations latentes d'un même document dont une partie du bruit de plus bas niveau est supprimée. Même si les représentations visibles sont éloignées, les représentations latentes ne doivent contenir que l'essence du document. Elles doivent donc avoir plus de points communs. Cela permet aux SDAE de créer un lien entre les deux représentations et de supprimer une partie supplémentaire du bruit introduit par le SRAP. La couche cachée centrale $h$ est le lien entre ces deux représentations. Elle est plus abstraite et moins bruitée que $h_{ASR}$.

\section{Conclusion}
\label{chap:sdae:sec:conclusion}

Ce chapitre propose une nouvelle représentation latente pour la compréhension de documents parlés. Elle utilise les caractéristiques issues d'une couche \anglicisme{Bottleneck} d'un autoencodeur profond. Ce réseau de neurones est initialisé par deux DAE appris sur des données d'origine différentes. Il exploite l'information provenant de documents corrompus de facto et de documents propres.

Cette représentation a pour effet de réduire l'impact des erreurs de transcription automatique sur la compréhension de documents parlés.
L'impact des erreurs du SRAP est réduit en entrainant une projection intermédiaire non linéaire qui lie les documents produits par le SRAP à ceux transcrits manuellement.

Des représentations latentes des deux types de documents (respectivement ASR et TRS) sont apprises préalablement et ne sont plus modifiées lors de l'entrainement de l'espace intermédiaire.
Cette décision est appuyée par nos expériences qui montrent un gain de $6\%$ comparé au système de référence et jusqu'à $10\%$ comparé aux méthodes qui adaptent l'ensemble des paramètres du réseau lors de l'apprentissage final. Ces résultats montrent que la représentation d'un même document apprise par les $\text{AE}_{\text{ASR}}$ et $\text{AE}_{\text{TRS}}$ contiennent des informations latentes d'ordre sémantique qui sont plus similaires que les représentations apparentes.

Dans la situation où les informations de supervision ne sont pas disponibles et que les temps de calcul ne sont pas une contrainte, alors l'utilisation d'un SAE est envisageable. En effet, il est capable de produire de bonnes représentations latentes, avec un gain de plus de $4\%$ par rapport au système de référence. Le SAE est seulement $1,2\%$ en dessous du SDAE, sans supervision, au prix d'un temps d'apprentissage deux fois plus important.

Nous envisageons trois possibilités intéressantes d'évolution pour ces travaux.

La première serait d'évaluer ce même système en passant les données à l'échelle. En effet, la taille du corpus de la tâche TID est limitée. Sur un corpus plus gros, l'écart entre les méthodes supervisées et non supervisées devrait se creuser. Un corpus annoté thématiquement, transcrit manuellement et de taille suffisamment importante pour faire une différence est rare.

La seconde ouverture que nous proposons concerne la représentation d'origine TF.IDF. Par exemple, utiliser des modèles graphiques probabilistes (comme la LDA~\cite{blei2003latent}, le modèle ``Author-Topic''~\cite{rosen2004author}), ou des réseaux de neurones capables de modéliser la structure des documents comme les réseaux LSTM~\cite{graves2012supervised} ou GRU~\cite{chung2014empirical} serait intéressant. En effet, la séquentialité des mots est un indice important pour caractériser les erreurs introduites par un SRAP.\\
Utiliser d'autres représentations textuelles permettrait aussi d'étudier le lien entre : les performances d'un réseau de neurones, le niveau d'abstraction (et de complexité) des données d'entrée et la quantité de données d'apprentissage disponibles. Même si ce n'est pas directement lié à la compréhension de la parole, c'est une piste d'étude intéressante.

La troisième possibilité serait d'introduire la supervision plus tôt dans la création des représentations des documents. Cette méthode est approfondie dans le Chapitre~\ref{chap:tdae}.
