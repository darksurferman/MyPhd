# Bilan des changement post Soutenance: 
## Hors Parties
- Correction de la page de garde 
    - ajout du jury   P.1
    - Ajout du sous-titre P.1
- ajout Page de remerciement  P.3
- Nettotage de la bibliographie P.159-183
    - suppression des doublons 
    - suppressio des d'arxiv
- Annexe : Ajout d'une note de page pour Arxiv P.190 #26
# Conclusion / intro
- introduction
    - Diverse corrections proposées par le jury et Typos (>= 1 phrases)
    - Précision sur la classification thématique de transcriptions P.11 L.19 (Fin du premier Paragraphe) & P.16 Derniére ligne.

- Ajout d'une Section Bilan et signficativités   P.141-147
    - Présentation des matrices de confusions P141-142 S10.1
    - Présentation des test de significativité P142-143 P10.1
    - Ajout du bilan global extraction du TDAE  P144-147 10.2
- Bilan et conclusion P.149
    - Diverse corrections proposées par le jury et Typos (>= 1 phrases)
## Partie 1
- Repr textuelle P.21
    - Diverse corrections proposées par le jury et Typos (>= 1 phrases)
    - Correction des formules selon les remarques du Jury P.23-24
    - Descriptions des Mesures d'informations P24-25 (paragraphe à cheval)
    - Normalisation de l'utilisation thèmes/topics pour la Section
- Réseaux de neurones P35
    - Diverse corrections proposées par le jury et Typos (>= 1 phrases)
    - Correction des références Arxiv P.35 P.36 P.43 P.48 P.49 P.51 
    - Rajout d'information sur CTC P49-50 S.3.6.1
## Partie 2
    - Intro Tache
        - Ajout des scores de confiances a 95% P.64 T.4.3
        - Diverse corrections proposées par le jury et Typos (>= 1 phrases)
    - Word2Dist
        - Correction des Refs
        - Diverse corrections proposées par le jury et Typos (>= 1 phrases)
        - Normalisation de l'utilisation Topics
    - SAE 
        - Ajout référence à la figure local oublié P.92 (1ere liste)
        - Diverse corrections proposées par le jury et Typos (>= 1 phrases)
## Partie 3
    - ATxLTS
        - Nettoyage des tableaux P.113 P.115
        - Uniformisation des Figures (Titres et légendes étaient en anglais) 
        - Diverse corrections proposées par le jury et Typos (>= 1 phrases)
    - SDAE
        - Diverse corrections proposées par le jury et Typos (>= 1 phrases)
        - Correction des nom des matrices  P.123-124 S.8.2.2
        - Précisions sur l'obtentions d'une taille de réseaux idéal P.124 (Avant 8.3)
    - TDAE
        - Diverse corrections proposées par le jury et Typos (>= 1 phrases)
        - Normalisation du Tableau P.137
        - Suppression de l'analyse comparatives aux section précedentes (déposé dans un chapitre de Bilan) Remplacé par un liant 1er paragraphe P.139